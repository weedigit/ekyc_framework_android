package wee.dev.libface.util;

import android.content.Context;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import wee.dev.libface.R;

public class LanguageHelper {
    static JSONObject languageData;

    public static void initLang(Context context,String language) throws IOException, JSONException {
        // Read content of language.json
        if(language.equals("VN")){
            String jsonData = readText(context, R.raw.language_vi);
            languageData = new JSONObject(jsonData);
        } else  {
            String jsonData = readText(context, R.raw.language_en);
            languageData = new JSONObject(jsonData);
        }
    }

    public static String getStringLanguage(String key){
        try {
            return languageData.getString(key);
        } catch (Exception e){
            return "not found key ";
        }
    }

    private static String readText(Context context, int resId) throws IOException {
        InputStream is = context.getResources().openRawResource(resId);
        BufferedReader br= new BufferedReader(new InputStreamReader(is));
        StringBuilder sb= new StringBuilder();
        String s= null;
        while((  s = br.readLine())!=null) {
            sb.append(s);
            sb.append("\n");
        }
        return sb.toString();
    }
}
