package wee.dev.libface.util

import android.content.Context
import android.graphics.Bitmap
import android.os.Handler
import android.os.HandlerThread
import android.util.Log
import androidx.renderscript.Allocation
import androidx.renderscript.Element
import androidx.renderscript.RenderScript
import androidx.renderscript.ScriptIntrinsicYuvToRGB
import com.homesoft.encoder.AvcEncoderConfig
import com.homesoft.encoder.FrameEncoder
import wee.dev.libface.base.VideoRecProcessListener
import wee.dev.libface.camera.CameraConfig
import java.io.File

class VideoUtils(val context: Context){
    companion object {
        const val TAG = "VideoUtils"

    }

    private val mPathVideo = "${context.externalCacheDir}/video"
    private var mThread = HandlerThread("VideoUtilThread")
    private var mThreadAddFrame = HandlerThread("AddFrameThread")
    private var mHandler: Handler? = null
    private var mHandlerAddFrame: Handler? =null
    private var frameEncoder: FrameEncoder? = null
    private var videoDir = ""
    private var isStarted = false
    private var yuvToRGBIntrinsic: ScriptIntrinsicYuvToRGB? = null
    private val yuvDatalength =
        CameraConfig.CAMERA_WIDTH * CameraConfig.CAMERA_HEIGHT * 3 / 2 // this is 12 bit per pixel
    private var aIn: Allocation? = null
    private var aOut: Allocation? = null
    private val rs = RenderScript.create(context)
    private val bmpOut = Bitmap.createBitmap(
        CameraConfig.CAMERA_HEIGHT,
        CameraConfig.CAMERA_WIDTH,
        Bitmap.Config.ARGB_8888
    )
    private var isPauseRec = false
    private var mListener: VideoRecProcessListener?=null
    private val listImage  = arrayListOf<ByteArray>()
    private var isAdding = false

    init {
        mThread.start()
        mThreadAddFrame.start()
        mHandler = Handler(mThread.looper)
        mHandlerAddFrame = Handler(mThreadAddFrame.looper)
        val file = File(mPathVideo)
        if (!file.exists()) {
            Log.e(TAG, "Create Folder: $mPathVideo")
            file.mkdirs()
        } else {
            file.deleteOnExit()
            file.mkdirs()
        }
        yuvToRGBIntrinsic = ScriptIntrinsicYuvToRGB.create(rs, Element.U8_4(rs))
        aIn = Allocation.createSized(rs, Element.U8(rs), yuvDatalength)
        aOut = Allocation.createFromBitmap(rs, bmpOut)
        yuvToRGBIntrinsic?.setInput(aIn)
    }

    fun onDestroy() {
        isPauseRec = true
        rs.finish()
        rs.destroy()
        bmpOut.recycle()
        aIn?.destroy()
        aOut?.destroy()
        yuvToRGBIntrinsic?.destroy()
        mThread.quitSafely()
        mThreadAddFrame.quitSafely()
    }

    fun onStart() {
        if(isStarted) return
        mHandler?.post {
            try {
                videoDir = "$mPathVideo/${System.currentTimeMillis()}.mp4"
                val file = File(videoDir)
                file.createNewFile()
                val config = AvcEncoderConfig(
                    videoDir,
                    CameraConfig.CAMERA_HEIGHT,
                    CameraConfig.CAMERA_WIDTH,
                    15f,
                    2000000
                )
                frameEncoder = FrameEncoder(config)
                frameEncoder!!.start()
                isStarted = true
                isPauseRec = false
                isAdding = false
                Log.e(TAG, "Start OK")
            } catch (e: Exception) {
                Log.e(TAG, "Error: ${e.message}")
            }
        }

    }

    /*fun pushFrame(bitmap: Bitmap) {
        if (!isStarted) return
        if(isPauseRec) return
        if(isProcessing) return
        isProcessing = true
        mHandler?.post {
            val copyBitmap = bitmap.copy(Bitmap.Config.RGB_565,true)
            mFrameEncoder?.createFrame(copyBitmap)
            Log.e(TAG, "Push Bitmap ${copyBitmap.width}x${copyBitmap.height}")
            isProcessing = false
        }
    }*/

    /*fun pushFrame(image: FirebaseVisionImage) {
        if (!isStarted) return
        if(isPauseRec) return
        if(isProcessing) return
        isProcessing = true
        mHandler?.post {
            val copyBitmap = image.bitmap
            mFrameEncoder?.createFrame(copyBitmap)
            Log.e(TAG, "Push Bitmap ${copyBitmap.width}x${copyBitmap.height}")
            isProcessing = false
        }
    }*/

    fun pushFrame(nv21: ByteArray) {
        if (!isStarted) return
        if(isPauseRec) return
        if(isAdding) return
        isAdding = true
        mHandler?.post {
            addFrameSingle(nv21).apply {
                if(!isStarted){
                    mHandlerAddFrame?.postDelayed({
                        try{
                            frameEncoder?.release()
                            Log.e(TAG, "OnDone: $videoDir")
                            mListener?.onDone(videoDir)
                        }catch (e: Exception){
                            Log.e(TAG, "OnDone: ${e.message}")
                            mListener?.onDone("")
                        }
                    },500)
                }else{
                    isAdding = false
                }
            }
        }
    }

    private fun addFrameSingle(frame: ByteArray){
        val bitmap = nv21toBitmap(frame)
        mHandlerAddFrame?.post {
            if(bitmap!=null && isStarted && !isPauseRec){
                try{
                    frameEncoder?.createFrame(bitmap)
                }catch (e: Exception){
                    Log.e("addFrameSingle","${e.message}")
                }
            }
        }
    }

    /*private fun addFrameStart(){
        if(isAddFrameStart) return
        isAddFrameStart = true
        mHandlerAddFrame?.post{
            Log.e(TAG, "Start Add Bitmap")
            while (listImage.size>0){
                val item = listImage.first()
                nv21toBitmap(item).apply {
                    listImage.remove(item)
                    if(this!=null) {
                        frameEncoder?.createFrame(this)
                        this.recycle()
                    }
                    Log.e(TAG, "Add Bitmap - Index: $getFrameIndex")
                    getFrameIndex++
                }
            }

            if(!isStarted){
                listImage.clear()
                frameEncoder?.release()
                rs.finish()
                Log.e(TAG, "OnDone: $videoDir")
                mListener?.onDone(videoDir)
            }else{
                isAddFrameStart = false
            }
        }
    }*/
    private val faceUtils = FaceUtils()
    private fun nv21toBitmap(nv21: ByteArray): Bitmap? {
        return try{
            val timeIn = System.currentTimeMillis()
            val rotateNV21 = faceUtils.rotateNV21(nv21,CameraConfig.CAMERA_WIDTH,CameraConfig.CAMERA_HEIGHT,270)
            /*val image = BitmapUtils.NV21toJPEG(rotateNV21,CameraConfig.CAMERA_HEIGHT,CameraConfig.CAMERA_WIDTH,100)
            val bitmap = BitmapFactory.decodeByteArray(image,0,image.size)*/
            aIn?.copyFrom(rotateNV21)
            yuvToRGBIntrinsic?.forEach(aOut)
            aOut?.copyTo(bmpOut)
            //Log.e("nv21toBitmap","Done.....[${System.currentTimeMillis()-timeIn}]")
            bmpOut
        }catch (e: Exception){
            Log.e("nv21toBitmap","${e.message}")
            null
        }
    }

    fun onPause(){
        isPauseRec = true
    }

    fun onDone(listener: VideoRecProcessListener) {
        if (!isStarted) return
        isStarted = false
        mListener = listener
        if(!isAdding){
            try{
                frameEncoder?.release()
                Log.e(TAG, "OnDone: $videoDir")
                mListener?.onDone(videoDir)
            }catch (e: Exception){
                Log.e(TAG, "OnDone: ${e.message}")
                mListener?.onDone("")
            }

        }
    }

}