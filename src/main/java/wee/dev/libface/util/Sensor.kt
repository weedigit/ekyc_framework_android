package wee.dev.libface.util

import android.content.Context
import android.hardware.Sensor
import android.hardware.SensorEvent
import android.hardware.SensorEventListener
import android.hardware.SensorManager

class Sensor : SensorEventListener {

    private var mContext: Context? = null

    private var sensorManager: SensorManager? = null

    private var listener: SensorListener? = null

    private var isCheckSensor = false

    constructor(context: Context, l: SensorListener) {
        mContext = context
        listener = l
    }

    fun createSensor() {
        sensorManager = mContext?.getSystemService(Context.SENSOR_SERVICE) as SensorManager
        sensorManager?.registerListener(
            this,
            sensorManager?.getDefaultSensor(Sensor.TYPE_GRAVITY),
            SensorManager.SENSOR_DELAY_NORMAL
        )
    }

    override fun onAccuracyChanged(sensor: Sensor?, accuracy: Int) {}

    override fun onSensorChanged(event: SensorEvent?) {
        if (isCheckSensor) return
        isCheckSensor = true
        if (checkSensorXY(event)) {
            listener?.checkSensor(true)
            isCheckSensor = false
        } else {
            listener?.checkSensor(false)
            isCheckSensor = false
        }
    }

    private fun checkSensorXY(sensor: SensorEvent?): Boolean {
//        axisX in -3f..3f && axisY in -3f..3f for horizontal
        val axisX = sensor?.values?.get(0) ?: return false
        val axisY = sensor?.values?.get(1) ?: return false
        return axisX in -5f..5f && axisY in 7f..13f/*-3f..3f && axisY in 8.5f..11f*/
    }

    fun destroySensor() {
        sensorManager?.unregisterListener(this)
    }

    interface SensorListener {
        fun checkSensor(bool: Boolean)
    }

}