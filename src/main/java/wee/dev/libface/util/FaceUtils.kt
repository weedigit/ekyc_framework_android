package wee.dev.libface.util

import android.content.Context
import android.graphics.*
import android.graphics.Bitmap.CompressFormat
import android.graphics.Bitmap.Config
import android.media.FaceDetector
import android.os.Environment
import android.os.Handler
import android.os.Looper
import android.util.DisplayMetrics
import android.util.Log
import android.widget.ImageView
import androidx.renderscript.Allocation
import androidx.renderscript.Element
import androidx.renderscript.RenderScript
import androidx.renderscript.ScriptIntrinsicBlur
import com.google.android.gms.vision.face.Landmark
import com.google.firebase.FirebaseApp
import com.google.firebase.ml.vision.FirebaseVision
import com.google.firebase.ml.vision.common.FirebaseVisionImage
import com.google.firebase.ml.vision.common.FirebaseVisionPoint
import com.google.firebase.ml.vision.face.FirebaseVisionFace
import com.google.firebase.ml.vision.face.FirebaseVisionFaceLandmark
import com.google.firebase.ml.vision.objects.FirebaseVisionObjectDetectorOptions
import wee.dev.libface.base.CropObjectListener
import wee.dev.libface.base.FacePointData
import wee.dev.libface.camera.CameraConfig
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.nio.ByteBuffer
import kotlin.math.*

class FaceUtils {
    companion object {
        const val MAX_FACE = 5
    }

    fun getFace(bitmap: Bitmap): Array<FaceDetector.Face?> {
        return try {
            val stream = ByteArrayOutputStream()
            bitmap.compress(CompressFormat.JPEG, 100, stream)
            val byteArray = stream.toByteArray()
            getFace(byteArray, bitmap.width, bitmap.height)
        } catch (e: Exception) {
            arrayOf()
        }
    }

    fun rotateBitmap(bitmap: Bitmap, degree: Float?): Bitmap {
        val matrix = Matrix()
        matrix.postRotate(degree!!)
        return Bitmap.createBitmap(bitmap, 0, 0, bitmap.width, bitmap.height, matrix, true)
    }

    fun getRectFace(face: FaceDetector.Face): Rect {
        val midPoint = PointF()
        face.getMidPoint(midPoint)
        val faceWidth = face.eyesDistance() * 2.3
        val faceHeight = face.eyesDistance() * 3
        val x = midPoint.x
        val y = midPoint.y
        val left = x - (faceWidth / 2)
        val top = y - (faceHeight / 2)
        val right = x + (faceWidth / 2)
        val bottom = y + (faceHeight / 2)
        return Rect(left.roundToInt(), top.roundToInt(), right.roundToInt(), bottom.roundToInt())
    }

    @Synchronized
    fun getRectDepthFace(faceRect: Rect): Rect {
        val leftCorner = 92
        val topCorner = 72
        val scale = 0.64
        val x = faceRect.exactCenterX() * scale + leftCorner / scale
        val y = faceRect.exactCenterY() * scale + topCorner
        val width = faceRect.width() * scale * 0.9
        val height = faceRect.height() * scale * 0.9
        val left = x - width / 2
        val top = y - height / 2
        val right = x + width / 2
        val bottom = y + height / 2
        return Rect(left.roundToInt(), top.roundToInt(), right.roundToInt(), bottom.roundToInt())
    }

    @Synchronized
    fun cropBitmapWithRect(bitmap: Bitmap, rect: Rect): Bitmap {
        var top = rect.top
        if (top < 0) {
            top = 0
        }
        //---
        var left = rect.left
        if (left < 0) {
            left = 0
        }
        //---
        val height = if(rect.height()>bitmap.height) bitmap.height else rect.height()
        //---
        val width = if(rect.width()>bitmap.width) bitmap.width else rect.width()
        //---
        var x = left
        var y = top
        if (x < 0) x = 0
        if (y < 0) y = 0
        try {
            return Bitmap.createBitmap(bitmap, x, y, width, height) ?: return bitmap
        } catch (ex: Exception) {
            Log.e("getFaceResult", ex.message.toString())
            return bitmap
        }
    }

    fun plusRect(rect: Rect,per: Float,maxW: Int, maxH: Int): Rect{
        val plusW = rect.width()*per
        val plusH = rect.height()*per
        var left = rect.left - plusW/2
        var top = rect.top - plusH/2
        var right = rect.right + plusW
        var bottom = rect.bottom + plusH
        if(right>maxW){
            right = maxW.toFloat()
        }
        if(bottom>maxH){
            bottom = maxH.toFloat()
        }
        if(left<0){
            left = 0f
        }
        if(top<0){
            top = 0f
        }
        return Rect(left.roundToInt(),top.roundToInt(),right.roundToInt(),bottom.roundToInt())
    }



    fun getFace(frame: ByteArray, width: Int, height: Int): Array<FaceDetector.Face?> {
        return try {
            val bitmapFactoryOptionsbfo = BitmapFactory.Options()
            bitmapFactoryOptionsbfo.inPreferredConfig = Bitmap.Config.RGB_565
            val mBitmap =
                BitmapFactory.decodeByteArray(frame, 0, frame.size, bitmapFactoryOptionsbfo)
            val myFace = arrayOfNulls<FaceDetector.Face>(MAX_FACE)
            val myFaceDetect = FaceDetector(width, height, MAX_FACE)
            myFaceDetect.findFaces(mBitmap, myFace)
            myFace
        } catch (e: Exception) {
            arrayOf()
        }
    }

    fun flipBimtap(src: Bitmap): Bitmap {
        val m = Matrix()
        m.preScale((-1).toFloat(), 1f)
        val dst: Bitmap = Bitmap.createBitmap(src, 0, 0, src.width, src.height, m, false)
        dst.density = DisplayMetrics.DENSITY_DEFAULT
        return dst
    }

    fun toGrayscale(bmpOriginal: Bitmap): Bitmap {
        val width: Int = bmpOriginal.width
        val height: Int = bmpOriginal.height
        val bmpGrayscale: Bitmap = Bitmap.createBitmap(width, height, Config.ARGB_8888)
        val c = Canvas(bmpGrayscale)
        val paint = Paint()
        val cm = ColorMatrix()
        cm.setSaturation(0f)
        val f = ColorMatrixColorFilter(cm)
        paint.colorFilter = f
        c.drawBitmap(bmpOriginal, 0f, 0f, paint)
        return bmpGrayscale
    }

    fun saveToInternalStorage(bitmapImage: Bitmap) {
        val folder = "FakeImage"
        val f = File(Environment.getExternalStorageDirectory(), folder)
        if (!f.exists()) {
            f.mkdirs()
        }
        val fileImg = "${f.absolutePath}/${System.currentTimeMillis()}.jpg"
        val fos = FileOutputStream(fileImg)
        try {
            // Use the compress method on the BitMap object to write image to the OutputStream
            bitmapImage.compress(CompressFormat.JPEG, 100, fos)
        } catch (e: Exception) {
            e.printStackTrace()
        } finally {
            try {
                fos.close()
            } catch (e: IOException) {
                e.printStackTrace()
            }
        }
    }

    fun combineImages(c: Bitmap, s: Bitmap): Bitmap? {
        var cs: Bitmap? = null
        val width: Int
        var height = 0
        if (c.width > s.width) {
            width = c.width + s.width
            height = c.height
        } else {
            width = s.width + s.width
            height = c.height
        }
        cs = Bitmap.createBitmap(
            width,
            height,
            Config.RGB_565
        )
        val comboImage = Canvas(cs)
        comboImage.drawBitmap(c, 0f, 0f, null)
        comboImage.drawBitmap(s, c.width.toFloat(), 0f, null)
        return cs
    }

    fun bitmapToByteArray(bitmap: Bitmap): ByteArray {
        val stream = ByteArrayOutputStream()
        bitmap.compress(CompressFormat.JPEG, 100, stream)
        val byteArray: ByteArray = stream.toByteArray()
        bitmap.recycle()
        return byteArray
    }

    fun rgb8ToArgb(rgb8: ByteArray, width: Int, height: Int): IntArray? {
        try {
            val frameSize = width * height
            val rgb = IntArray(frameSize)
            var index = 0
            for (j in 0 until height) {
                for (i in 0 until width) {
                    val B = rgb8[3 * index].toInt()
                    val G = rgb8[3 * index + 1].toInt()
                    val R = rgb8[3 * index + 2].toInt()
                    rgb[index] = (R and 0xff) or (G and 0xff shl 8) or (B and 0xff shl 16)
                    index++
                }
            }
            return rgb
        } catch (e: Exception) {
            Log.e("rgb8ToArgb", "Error: ${e.message}")
            return null
        }

    }

    private fun getBitmap(buffer: ByteBuffer, width: Int, height: Int): Bitmap {
        buffer.rewind()
        val bitmap = Bitmap.createBitmap(width, height, Config.ARGB_8888)
        buffer.position(0)
        bitmap.copyPixelsFromBuffer(buffer)
        /*val c =  Canvas(bitmap)
        val paint = Paint()
        val cm = ColorMatrix()
        cm.setSaturation(0f)
        val f = ColorMatrixColorFilter(cm)
        paint.colorFilter = f
        c.drawBitmap(bitmap, 0f, 0f, paint)*/
        return bitmap
    }

    fun isFrontFace(face: FirebaseVisionFace?): Boolean{
        face?:return false
        return checkLandMark(face)
    }

    fun getLargestFace(faces: List<FirebaseVisionFace>): FirebaseVisionFace? {
        return if (faces.isNotEmpty()) {
            var largestFace = faces.first()
            faces.forEach { face->
                if (getFaceSize(face) > CameraConfig.MIN_FACE_SIZE) {
                    if (getFaceSize(largestFace) < getFaceSize(face)) {
                        largestFace = face
                    }
                }
            }
            return largestFace
        }else{
            null
        }
    }

    fun getLargestFaceDual(faces: List<FirebaseVisionFace>): FirebaseVisionFace? {
        var largestFace: FirebaseVisionFace? = null
        val mList = ArrayList<String>()
        if (faces.isNotEmpty()) {
            for (i in faces.indices) {
                mList.add("" + faces[i].trackingId)
                val face = faces[i]
                if (getFaceSize(face) > 110 && checkLandMark(face)&& checkZoneFace(face)) {
                    if (largestFace == null || getFaceSize(largestFace) < getFaceSize(face)) {
                        largestFace = face
                    }
                }
            }
        }

        return largestFace
    }

    fun getLargestFaceDualInf(faces: List<FirebaseVisionFace>): FirebaseVisionFace? {
        var largestFace: FirebaseVisionFace? = null
        val mList = ArrayList<String>()
        if (faces.isNotEmpty()) {
            for (i in faces.indices) {
                mList.add("" + faces[i].trackingId)
                val face = faces[i]
                if (getFaceSize(face) > 90 && checkLandMark(face)) {
                    if (largestFace == null || getFaceSize(largestFace) < getFaceSize(face)) {
                        largestFace = face
                    }
                }
            }
        }

        return largestFace
    }

    private fun checkLandMark(face:FirebaseVisionFace?):Boolean{
        face?:return false
        return face.headEulerAngleY in CameraConfig.FRONT_FACE_VALUE && face.headEulerAngleZ in CameraConfig.FRONT_FACE_VALUE
    }

    fun checkZoneFace(face: FirebaseVisionFace?):Boolean{
        face?:return false
        val centerX = face.boundingBox.centerX()
        val centerY = face.boundingBox.centerY()
        val sizeZone = CameraConfig.pSizeTrackingZone

        val xa = CameraConfig.CAMERA_HEIGHT/2 - sizeZone.width
        val xb = CameraConfig.CAMERA_HEIGHT/2 + sizeZone.width
        val ya = CameraConfig.CAMERA_WIDTH/2.5f - sizeZone.height
        val yb = CameraConfig.CAMERA_WIDTH/2.5f + sizeZone.height
        val inZone =  centerX in xa..xb && centerY.toFloat() in ya..yb
        //Log.e("CheckZoneFace","X: $centerX - Y: $centerY - Zone: $xa .. $xb $ya .. $yb - InZone: $inZone")
        return inZone
    }

    fun checkZoneStartFace(face: FirebaseVisionFace?):Boolean{
        face?:return false
        val centerX = face.boundingBox.centerX()
        val centerY = face.boundingBox.centerY()
        val sizeZone = CameraConfig.pSizeStartZone

        /*val xa = CameraConfig.CAMERA_HEIGHT/2 - sizeZone.width
        val xb = CameraConfig.CAMERA_HEIGHT/2 + sizeZone.width
        val ya = CameraConfig.CAMERA_WIDTH/2.5f - sizeZone.height
        val yb = CameraConfig.CAMERA_WIDTH/2.5f + sizeZone.height*/
        val xa = CameraConfig.CAMERA_HEIGHT / 2.6 - sizeZone.width
        val xb = CameraConfig.CAMERA_HEIGHT / 1.6 + sizeZone.width
        val ya = CameraConfig.CAMERA_WIDTH / 2.6f - sizeZone.height
        val yb = CameraConfig.CAMERA_WIDTH / 2.3f + sizeZone.height
        val inZone =  centerX in xa..xb && centerY.toFloat() in ya..yb
        //Log.e("CheckZoneFace","X: $centerX - Y: $centerY - Zone: $xa .. $xb $ya .. $yb - InZone: $inZone")
        return inZone
    }

    fun getFaceSize(face: FirebaseVisionFace?): Int {
        face ?: return 0
        val bounds = face.boundingBox
        return bounds.width()
    }

    fun distancePoint(a : Point, b: Point): Float{
        return sqrt((a.x.toDouble() - b.x.toDouble()).pow(2.0) + (a.y.toDouble() - b.y.toDouble()).pow(2.0)).toFloat()
    }

    private fun convertPoint(oldPoint: FirebaseVisionPoint?, faceRect: Rect, plus: Int): Point {
        oldPoint ?: return Point(0, 0)
        val old_X = oldPoint.x.roundToInt()
        val old_Y = oldPoint.y.roundToInt()
        val new_X = old_X - faceRect.left + plus / 2
        val new_Y = old_Y - faceRect.top + plus / 2
        return Point(new_X, new_Y)
    }

    private fun getDataFace(face: FirebaseVisionFace, plus: Int): FacePointData {
        val leftTop = convertPoint(
            FirebaseVisionPoint(
                face.boundingBox.left.toFloat(),
                face.boundingBox.top.toFloat(),
                0f
            ), face.boundingBox, plus
        )
        val rightBottom = convertPoint(
            FirebaseVisionPoint(
                face.boundingBox.right.toFloat(),
                face.boundingBox.bottom.toFloat(),
                0f
            ), face.boundingBox, plus
        )

        val newRect = Rect(leftTop.x,leftTop.y,rightBottom.x,rightBottom.y)

        val leftEye = convertPoint(
            face.getLandmark(FirebaseVisionFaceLandmark.LEFT_EYE)?.position,
            face.boundingBox,
            plus
        )
        val rightEye = convertPoint(
            face.getLandmark(FirebaseVisionFaceLandmark.RIGHT_EYE)?.position,
            face.boundingBox,
            plus
        )
        val leftMouth = convertPoint(
            face.getLandmark(FirebaseVisionFaceLandmark.MOUTH_LEFT)?.position,
            face.boundingBox,
            plus
        )
        val rightMouth = convertPoint(
            face.getLandmark(FirebaseVisionFaceLandmark.MOUTH_RIGHT)?.position,
            face.boundingBox,
            plus
        )
        val nose = convertPoint(
            face.getLandmark(FirebaseVisionFaceLandmark.NOSE_BASE)?.position,
            face.boundingBox,
            plus
        )
        return FacePointData(newRect,rightEye, leftEye, nose, rightMouth, leftMouth)
    }

    fun drawFaceDataToBitmap(facePointData: FacePointData, faceBitmap: Bitmap): Bitmap {
        val canvas = Canvas(faceBitmap)
        val paint = Paint()
        paint.color = Color.RED
        paint.style = Paint.Style.STROKE
        paint.strokeWidth = 5f
        canvas.drawPoint(
            facePointData.LeftEye.x.toFloat(),
            facePointData.LeftEye.y.toFloat(),
            paint
        )
        canvas.drawPoint(
            facePointData.RightEye.x.toFloat(),
            facePointData.RightEye.y.toFloat(),
            paint
        )
        canvas.drawPoint(
            facePointData.Leftmouth.x.toFloat(),
            facePointData.Leftmouth.y.toFloat(),
            paint
        )
        canvas.drawPoint(
            facePointData.Rightmouth.x.toFloat(),
            facePointData.Rightmouth.y.toFloat(),
            paint
        )
        canvas.drawPoint(facePointData.Nose.x.toFloat(), facePointData.Nose.y.toFloat(), paint)

        canvas.drawRect(facePointData.faceRect,paint)
        canvas.save()
        return faceBitmap
    }

    fun drawRectToBitmap(rect: Rect, faceBitmap: Bitmap): Bitmap {
        val canvas = Canvas(faceBitmap)
        val paint = Paint()
        paint.color = Color.RED
        paint.style = Paint.Style.STROKE
        paint.strokeWidth = 5f
        canvas.drawRect(rect,paint)
        canvas.save()
        return faceBitmap
    }

    fun cropFaceFromBitmap(context: Context, imageView: ImageView, bitmap:Bitmap){
        FirebaseApp.initializeApp(context).apply {
            val faceDetector = FirebaseVision.getInstance().getVisionFaceDetector(CameraConfig.MLKitFaceOption)
            faceDetector.detectInImage(FirebaseVisionImage.fromBitmap(bitmap)).addOnSuccessListener {
                val largestFace = getLargestFace(it)
                if(largestFace!=null){
                    val faceCrop = cropBitmapWithRect(bitmap,largestFace.boundingBox)
                    Handler(Looper.getMainLooper()).post {
                        imageView.setImageBitmap(faceCrop)
                    }
                }
            }
        }
    }

    fun cropObjectFromBitmap(context:Context,bitmap: Bitmap, listener: CropObjectListener){
        FirebaseApp.initializeApp(context).apply {
            val options = FirebaseVisionObjectDetectorOptions.Builder()
                .setDetectorMode(FirebaseVisionObjectDetectorOptions.SINGLE_IMAGE_MODE)
                .enableMultipleObjects()
                .enableClassification()
                .build()
            val objectDetector = FirebaseVision.getInstance().getOnDeviceObjectDetector(options)
            objectDetector.processImage(FirebaseVisionImage.fromBitmap(bitmap))
                .addOnSuccessListener {objects ->
                    objects.forEach {
                        Log.e("ObjectDetected","${it.boundingBox}")
                    }
                    listener.onResult(bitmap)
                }
                .addOnFailureListener{
                    Log.e("ObjectDetected","${it.message}")
                    listener.onResult(bitmap)
                }

        }
    }

    fun cropObjectFromImage(context:Context, image: FirebaseVisionImage, listener: CropObjectListener){
        FirebaseApp.initializeApp(context).apply {
            /*val options = FirebaseVisionObjectDetectorOptions.Builder()
                .setDetectorMode(FirebaseVisionObjectDetectorOptions.SINGLE_IMAGE_MODE)
                .enableMultipleObjects()
                .enableClassification()
                .build()*/
            val objectDetector = FirebaseVision.getInstance().onDeviceObjectDetector
            objectDetector.processImage(image)
                .addOnSuccessListener {objects ->
                    if(objects.isNotEmpty()){
                        var largest = objects.first()
                        objects.forEach {
                            Log.e("ObjectDetected","${it.boundingBox}")
                            if(largest.boundingBox.width()<it.boundingBox.width()){
                                largest = it
                            }
                        }
                        val bitmapCrop = cropBitmapWithRect(image.bitmap,largest.boundingBox)
                        listener.onResult(bitmapCrop)
                    }else{
                        listener.onResult(image.bitmap)
                    }
                }
                .addOnFailureListener{
                    Log.e("ObjectDetected","${it.message}")
                    listener.onResult(image.bitmap)
                }

        }
    }

    private fun getBlurBitmap(context: Context, nv21: ByteArray): Bitmap? {
        val byteArr =
            BitmapUtils.NV21toJPEG(nv21, CameraConfig.CAMERA_WIDTH, CameraConfig.CAMERA_HEIGHT, 40)
        val bitmap = BitmapFactory.decodeByteArray(byteArr,0,byteArr.size)
        // Log.e("getBitmapFromNV21", "Bitmap ${bitmap.width} - ${bitmap.height}")
        val blurBitmap = blurBitmap(context, bitmap)
        return blurBitmap
    }

    private fun blurBitmap(ctx: Context?, image: Bitmap): Bitmap? {
        val width = (image.width * 1f).roundToInt()
        val height = (image.height * 1f).roundToInt()
        val inputBitmap = Bitmap.createScaledBitmap(image, width, height, false)
        val outputBitmap = Bitmap.createBitmap(inputBitmap)
        val rs: RenderScript = RenderScript.create(ctx)
        val theIntrinsic: ScriptIntrinsicBlur = ScriptIntrinsicBlur.create(rs, Element.U8_4(rs))
        val tmpIn: Allocation = Allocation.createFromBitmap(rs, inputBitmap)
        val tmpOut: Allocation = Allocation.createFromBitmap(rs, outputBitmap)
        theIntrinsic.setRadius(10f)
        theIntrinsic.setInput(tmpIn)
        theIntrinsic.forEach(tmpOut)
        tmpOut.copyTo(outputBitmap)
        return outputBitmap
    }

    fun rotateNV21(yuv: ByteArray, width: Int, height: Int, rotation: Int): ByteArray? {
        if (rotation == 0) return yuv
        if(rotation % 90 != 0 || rotation < 0 || rotation > 270) {
            return null
        }
        val output = ByteArray(yuv.size)
        val frameSize = width * height
        val swap = rotation % 180 != 0
        val xFlip = rotation % 270 != 0
        val yFlip = rotation >= 180
        for (j in 0 until height) {
            for (i in 0 until width) {
                val yIn = j * width + i
                val uIn = frameSize + (j shr 1) * width + (i and 1.inv())
                val vIn = uIn + 1
                val wOut = if (swap) height else width
                val hOut = if (swap) width else height
                val iSwapped = if (swap) j else i
                val jSwapped = if (swap) i else j
                val iOut = if (xFlip) wOut - iSwapped - 1 else iSwapped
                val jOut = if (yFlip) hOut - jSwapped - 1 else jSwapped
                val yOut = jOut * wOut + iOut
                val uOut = frameSize + (jOut shr 1) * wOut + (iOut and 1.inv())
                val vOut = uOut + 1
                output[yOut] = (0xff and yuv[yIn].toInt()).toByte()
                output[uOut] = (0xff and yuv[uIn].toInt()).toByte()
                output[vOut] = (0xff and yuv[vIn].toInt()).toByte()
            }
        }
        return output
    }

    fun getCenterPoint(point1: Point, point2: Point): Point {
        val disX = abs((point1.x - point2.x)) / 2
        val x = if (point1.x > point2.x) {
            point2.x + disX
        } else {
            point1.x + disX
        }
        val disY = abs((point1.y - point2.y)) / 2
        val y = if (point1.y > point2.y) {
            point2.y + disY
        } else {
            point1.y + disY
        }
        return Point(x, y)
    }
    
    fun getFaceDegreeY(pointEyeLeft: Point, pointEyeRight: Point, pointNose: Point, pointMouthLeft: Point, pointMouthRight: Point): Float{
        val pointCenterEye =getCenterPoint(pointEyeLeft, pointEyeRight)
        val pointCenterMouth = getCenterPoint(pointMouthLeft, pointMouthRight)
        val pointCenterY = getCenterPoint(pointCenterEye, pointCenterMouth)
        val rY = distancePoint(pointCenterEye, pointCenterY)
        val disOMY = distancePoint(Point(pointCenterY.x, pointNose.y), pointCenterY)
        val angleDataY = disOMY / rY
        val angleY = acos(angleDataY)
        return if (pointNose.y < pointCenterY.y) (90 - angleY * (180 / PI).toFloat()) else -(90 - angleY * (180 / PI).toFloat())
    }

    fun getFaceDegreeX(pointEyeLeft: Point, pointEyeRight: Point, pointNose: Point, pointMouthLeft: Point, pointMouthRight: Point): Float{
        val pointCenterEyeMouthLeft = getCenterPoint(pointEyeLeft,pointMouthLeft)
        val pointCenterEyeMouthRight = getCenterPoint(pointEyeRight,pointMouthRight)
        val pointCenterX = getCenterPoint(pointCenterEyeMouthLeft,pointCenterEyeMouthRight)
        val rX = distancePoint(pointCenterEyeMouthLeft,pointCenterX)
        val disOMX = distancePoint(Point(pointNose.x,pointCenterEyeMouthLeft.y),pointCenterX)
        val angleDataX = disOMX/rX
        val angleX = acos(angleDataX)
        return if (pointNose.x > pointCenterX.x) (90 - angleX * (180 / PI).toFloat()) else -(90 - angleX * (180 / PI).toFloat())
    }

    private fun getFaceDegreeX(largestFace: FirebaseVisionFace): Float {
        return try {
            val nosePoint = largestFace.getLandmark(Landmark.NOSE_BASE)
            val eyeLeft = largestFace.getLandmark(Landmark.LEFT_EYE)
            val eyeRight = largestFace.getLandmark(Landmark.RIGHT_EYE)
            val bottomMouth = largestFace.getLandmark(Landmark.BOTTOM_MOUTH)
            val leftMouth = largestFace.getLandmark(Landmark.LEFT_MOUTH)
            val rightMouth = largestFace.getLandmark(Landmark.RIGHT_MOUTH)
            return if (nosePoint != null && eyeLeft != null
                && eyeRight != null && bottomMouth != null
                && leftMouth != null && rightMouth != null
            ) {
                val pointNose =
                    Point(nosePoint.position.x.roundToInt(), nosePoint.position.y.roundToInt())
                val pointEyeLeft =
                    Point(eyeLeft.position.x.roundToInt(), eyeLeft.position.y.roundToInt())
                val pointEyeRight =
                    Point(eyeRight.position.x.roundToInt(), eyeRight.position.y.roundToInt())
                val pointMouthLeft =
                    Point(leftMouth.position.x.roundToInt(), leftMouth.position.y.roundToInt())
                val pointMouthRight =
                    Point(rightMouth.position.x.roundToInt(), rightMouth.position.y.roundToInt())
                getFaceDegreeX(pointEyeLeft,pointEyeRight,pointNose,pointMouthLeft,pointMouthRight)
            }else{
                0f
            }
        }catch (e: Exception){
            0f
        }
    }

    private fun getFaceDegreeY(largestFace: FirebaseVisionFace): Float {
        return try {
            val nosePoint = largestFace.getLandmark(Landmark.NOSE_BASE)
            val eyeLeft = largestFace.getLandmark(Landmark.LEFT_EYE)
            val eyeRight = largestFace.getLandmark(Landmark.RIGHT_EYE)
            val bottomMouth = largestFace.getLandmark(Landmark.BOTTOM_MOUTH)
            val leftMouth = largestFace.getLandmark(Landmark.LEFT_MOUTH)
            val rightMouth = largestFace.getLandmark(Landmark.RIGHT_MOUTH)
            return if (nosePoint != null && eyeLeft != null
                && eyeRight != null && bottomMouth != null
                && leftMouth != null && rightMouth != null
            ) {
                val pointNose =
                    Point(nosePoint.position.x.roundToInt(), nosePoint.position.y.roundToInt())
                val pointEyeLeft =
                    Point(eyeLeft.position.x.roundToInt(), eyeLeft.position.y.roundToInt())
                val pointEyeRight =
                    Point(eyeRight.position.x.roundToInt(), eyeRight.position.y.roundToInt())
                val pointMouthLeft =
                    Point(leftMouth.position.x.roundToInt(), leftMouth.position.y.roundToInt())
                val pointMouthRight =
                    Point(rightMouth.position.x.roundToInt(), rightMouth.position.y.roundToInt())
                getFaceDegreeY(pointEyeLeft,pointEyeRight,pointNose,pointMouthLeft,pointMouthRight)
            }else{
                0f
            }
        }catch (e: Exception){
            0f
        }
    }
}