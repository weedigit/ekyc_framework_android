package wee.dev.libface.util

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Bitmap
import android.graphics.Color
import android.graphics.Rect
import android.os.Build
import android.renderscript.*
import android.util.Log
import androidx.annotation.ColorInt
import androidx.annotation.RequiresApi
import wee.dev.libface.R

object BlurrinessRenderScript {

    private val CLASSIC_MATRIX = floatArrayOf(
            -1.0f, -1.0f, -1.0f,
            -1.0f, 8.0f, -1.0f,
            -1.0f, -1.0f, -1.0f
    )

    @SuppressLint("ResourceType")
    @RequiresApi(Build.VERSION_CODES.KITKAT)
    fun runDetection(context: Context, sourceBitmap: Bitmap, rect: Rect): Boolean {
        val rs = RenderScript.create(context)
        val bitmapFace = Bitmap.createBitmap(sourceBitmap,rect.left,rect.top,rect.width(),rect.height())
        val smootherBitmap = Bitmap.createBitmap(bitmapFace.width,
            bitmapFace.height,
            bitmapFace.config
        )
        val blurIntrinsic = ScriptIntrinsicBlur.create(rs, Element.RGBA_8888(rs))
        val source = Allocation.createFromBitmap(rs,
            bitmapFace,
                Allocation.MipmapControl.MIPMAP_NONE,
                Allocation.USAGE_SHARED
        )
        val blurTargetAllocation = Allocation.createFromBitmap(rs,
                smootherBitmap,
                Allocation.MipmapControl.MIPMAP_NONE,
                Allocation.USAGE_SHARED
        )
        blurIntrinsic.apply {
            setRadius(1f)
            setInput(source)
            forEach(blurTargetAllocation)
        }
        blurTargetAllocation.copyTo(smootherBitmap)


        val greyscaleBitmap = Bitmap.createBitmap(bitmapFace.width,
            bitmapFace.height,
            bitmapFace.config
        )
        val smootherInput = Allocation.createFromBitmap(rs,
                smootherBitmap,
                Allocation.MipmapControl.MIPMAP_NONE,
                Allocation.USAGE_SHARED
        )
        val greyscaleTargetAllocation = Allocation.createFromBitmap(rs,
                greyscaleBitmap,
                Allocation.MipmapControl.MIPMAP_NONE,
                Allocation.USAGE_SHARED
        )

        val colorIntrinsic = ScriptIntrinsicColorMatrix.create(rs)
        colorIntrinsic.setGreyscale()
        colorIntrinsic.forEach(smootherInput, greyscaleTargetAllocation)
        greyscaleTargetAllocation.copyTo(greyscaleBitmap)

        val edgesBitmap = Bitmap.createBitmap(bitmapFace.width,
            bitmapFace.height,
            bitmapFace.config
        )
        val greyscaleInput = Allocation.createFromBitmap(rs,
                greyscaleBitmap,
                Allocation.MipmapControl.MIPMAP_NONE,
                Allocation.USAGE_SHARED
        )
        val edgesTargetAllocation = Allocation.createFromBitmap(rs,
                edgesBitmap,
                Allocation.MipmapControl.MIPMAP_NONE,
                Allocation.USAGE_SHARED
        )

        val convolve = ScriptIntrinsicConvolve3x3.create(rs, Element.U8_4(rs))
        convolve.setInput(greyscaleInput)
        convolve.setCoefficients(CLASSIC_MATRIX) // Or use others
        convolve.forEach(edgesTargetAllocation)
        edgesTargetAllocation.copyTo(edgesBitmap)

        @ColorInt val mostLuminousColor = mostLuminousColorFromBitmap(edgesBitmap)
        val colorHex = "#" + Integer.toHexString(mostLuminousColor)
        val isBlurry = mostLuminousColor > Color.parseColor(context.getString(R.color.colorCheck))
        /*Demo threshold
        Note - in Android, Color.BLACK is -16777216 and Color.WHITE is -1, so range is somewhere in between. Higher is more luminous
        Toast.makeText(context, output, Toast.LENGTH_LONG).show()Log*/
        Log.d("Luminous", "isBlurry: $isBlurry - Luminous Color: $colorHex")
        return isBlurry
    }

    /**
     * Resolves the most luminous color pixel in a given bitmap.
     *
     * @param bitmap Source bitmap.
     * @return The most luminous color pixel in the `bitmap`
     */
    @ColorInt
    fun mostLuminousColorFromBitmap(bitmap: Bitmap): Int {
        bitmap.setHasAlpha(false)
        val pixels = IntArray(bitmap.height * bitmap.width)
        bitmap.getPixels(pixels, 0, bitmap.width, 0, 0, bitmap.width, bitmap.height)

        @ColorInt var mostLuminousColor = Color.BLACK

        for (pixel in pixels) {
            if (pixel > mostLuminousColor) {
                mostLuminousColor = pixel
            }
        }
        return mostLuminousColor
    }

}