package wee.dev.libface.util

import android.content.Context
import android.content.res.Resources
import android.graphics.Paint
import android.graphics.Path
import android.graphics.Point
import android.graphics.RectF
import android.media.MediaPlayer
import android.util.Log
import android.view.View
import android.view.WindowManager
import androidx.annotation.ColorRes
import androidx.core.content.ContextCompat
import wee.dev.libface.R
import wee.dev.libface.util.extension.postUI

object ScreenUtils {
    var widthScreen = 0
    var heightScreen = 0

    fun getScreenWidthFloat(c: Context): Float {
        return getScreenWidth(c).toFloat()
    }

    fun dpToPx(dp: Int): Int {
        return (dp * Resources.getSystem().displayMetrics.density).toInt()
    }

    fun dpToPxFloat(dp: Int): Float {
        return dp * Resources.getSystem().displayMetrics.density
    }

    fun pxToDp(px: Float): Float {
        return (px / Resources.getSystem().displayMetrics.density).toInt().toFloat()
    }

    fun pxToDpInt(px: Int): Int {
        return (px / Resources.getSystem().displayMetrics.density).toInt()
    }


    fun getScreenHeight(c: Context): Int {
        if (heightScreen == 0) {
            val wm = c.getSystemService(Context.WINDOW_SERVICE) as WindowManager
            val display = wm.defaultDisplay
            val size = Point()
            display.getSize(size)
            heightScreen = size.y
        }
        return heightScreen
    }

    fun getScreenWidth(c: Context): Int {
        if (widthScreen == 0) {
            val wm = c.getSystemService(Context.WINDOW_SERVICE) as WindowManager
            val display = wm.defaultDisplay
            val size = Point()
            display.getSize(size)
            widthScreen = size.x
        }
        //---
        return widthScreen
    }

    fun CreatePath(centerX : Float, centerY : Float, radiusTopLeft : Float) : Path {
        val path = Path()
        path.addCircle(centerX / 2, centerY / 2, radiusTopLeft, Path.Direction.CW)
        return path
    }

    fun CreatePaint(context : Context, @ColorRes color : Int = R.color.white, stroke : Float = 30f) : Paint {
        val paint = Paint()
        paint.color = ContextCompat.getColor(context, color)
        paint.strokeWidth = stroke
        paint.style = Paint.Style.STROKE
        paint.strokeCap = Paint.Cap.ROUND
        return paint
    }

    fun createPaintArrow(context : Context, @ColorRes color : Int = R.color.white, stroke : Float = 20f) : Paint {
        val paint = Paint()
        paint.color = ContextCompat.getColor(context, color)
        return paint
    }

    fun CreateOval(paint: Paint, centerX: Float, centerY: Float, radiusTopLeft: Float) : RectF {
        val oval = RectF()
        paint.style = Paint.Style.STROKE
        oval.set(centerX - radiusTopLeft, centerY - radiusTopLeft, centerX + radiusTopLeft, centerY + radiusTopLeft)
        return oval
    }

    fun playAudio(context: Context){
        try{
            val mp3 = MediaPlayer.create(context, R.raw.ting)
            mp3.start()
        }catch (e : Exception){
            Log.e("playAudio","${e.message}")
        }
    }

    fun show(vararg view: View) {
        for (v in view) postUI { v.visibility = View.VISIBLE }
    }

    fun gone(vararg view : View){
        for(v in view)postUI { v.visibility = View.GONE }
    }

    fun hide(vararg view : View){
        for(v in view)postUI { v.visibility = View.INVISIBLE }
    }

}
