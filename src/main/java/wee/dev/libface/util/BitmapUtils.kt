package wee.dev.libface.util

import android.content.Context
import android.graphics.*
import android.media.Image
import android.os.Build
import androidx.renderscript.Allocation
import androidx.renderscript.Element
import androidx.renderscript.RenderScript
import androidx.renderscript.ScriptIntrinsicBlur
import java.io.ByteArrayOutputStream
import java.io.IOException
import java.lang.reflect.InvocationTargetException
import kotlin.math.roundToInt


object BitmapUtils {
    private val TAG = BitmapUtils::class.java.simpleName
    private fun convert(
        raw: ByteArray,
        pixels: IntArray,
        exposureCompensation: Double
    ) {
        if (exposureCompensation != null) {
            for (i in raw.indices) {
                val grey = Math.min(((0xFF and raw[i].toInt()) * exposureCompensation).toInt(), 255)
                pixels[i] = -0x1000000 or 0x010101 * grey
            }
        } else {
            for (i in raw.indices) {
                val grey = 0xFF and raw[i].toInt()
                pixels[i] = -0x1000000 or 0x010101 * grey
            }
        }
    }

    fun toBitmap(
        bitmap: Bitmap,
        pixels: IntArray,
        raw: ByteArray,
        exposureCompensation: Double
    ): Bitmap {
        val width = bitmap.width
        val height = bitmap.height
        convert(raw, pixels, exposureCompensation)
        bitmap.setPixels(pixels, 0, width, 0, 0, width, height)
        return bitmap
    }

    fun toBitmap(
        width: Int,
        height: Int,
        raw: ByteArray,
        exposureCompensation: Double
    ): Bitmap {
        val bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888)
        val pixels = IntArray(raw.size)
        return toBitmap(bitmap, pixels, raw, exposureCompensation)
    }

    fun getBGR(yuvImage: YuvImage): Bitmap {
        val outStream = ByteArrayOutputStream()
        yuvImage.compressToJpeg(
            Rect(
                0,
                0,
                yuvImage.width,
                yuvImage.height
            ), 100, outStream
        ) // make JPG
        return BitmapFactory.decodeByteArray(outStream.toByteArray(), 0, outStream.size())
    }

    fun rotate(bmpImage: Bitmap, degrees: Int): Bitmap {
        val matrix = Matrix()
        matrix.postRotate(degrees.toFloat())
        matrix.postScale(-1f, 1f)
        return Bitmap.createBitmap(
            bmpImage,
            0,
            0,
            bmpImage.width,
            bmpImage.height,
            matrix,
            true
        )
    }

    fun crop(source: Bitmap?, aspectWidth: Int, aspectHeight: Int): Bitmap? {
        val sourceWidth = source!!.width
        val sourceHeight = source.height
        var width = sourceWidth
        var height = width * aspectHeight / aspectWidth
        var x = 0
        var y = (sourceHeight - height) / 2
        if (height > sourceHeight) {
            height = sourceHeight
            width = height * aspectWidth / aspectHeight
            x = (sourceWidth - width) / 2
            y = 0
        }
        val bitmap: Bitmap
        if (x != 0 || y != 0 || source.width != width || source.height != height) {
            bitmap = Bitmap.createBitmap(source, x, y, width, height)
            source.recycle()
        } else return source
        return bitmap
    }

    fun calculateInSampleSize(options: BitmapFactory.Options, reqWidth: Int, reqHeight: Int): Int {
        val height = options.outHeight
        val width = options.outWidth
        var inSampleSize = 1
        if (height > reqHeight || width > reqWidth) {
            val halfHeight = height / 2
            val halfWidth = width / 2
            while (halfHeight / inSampleSize > reqHeight && halfWidth / inSampleSize > reqWidth) {
                inSampleSize *= 2
            }
        }
        return inSampleSize
    }

    @Throws(IOException::class)
    private fun getExifOrientation(src: String): Int {
        var orientation = 1
        try {
            /**
             * if your are targeting only api level >= 5
             * ExifInterface exif = new ExifInterface(src);
             * orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, 1);
             */
            if (Build.VERSION.SDK_INT >= 5) {
                val exifClass =
                    Class.forName("android.media.ExifInterface")
                val exifConstructor = exifClass.getConstructor(
                    *arrayOf<Class<*>>(
                        String::class.java
                    )
                )
                val exifInstance =
                    exifConstructor.newInstance(*arrayOf<Any>(src))
                val getAttributeInt = exifClass.getMethod(
                    "getAttributeInt", *arrayOf<Class<*>?>(
                        String::class.java, Int::class.javaPrimitiveType
                    )
                )
                val tagOrientationField =
                    exifClass.getField("TAG_ORIENTATION")
                val tagOrientation = tagOrientationField[null] as String
                orientation = getAttributeInt.invoke(
                    exifInstance,
                    *arrayOf<Any>(tagOrientation, 1)
                ) as Int
            }
        } catch (e: ClassNotFoundException) {
            e.printStackTrace()
        } catch (e: SecurityException) {
            e.printStackTrace()
        } catch (e: NoSuchMethodException) {
            e.printStackTrace()
        } catch (e: IllegalArgumentException) {
            e.printStackTrace()
        } catch (e: InstantiationException) {
            e.printStackTrace()
        } catch (e: IllegalAccessException) {
            e.printStackTrace()
        } catch (e: InvocationTargetException) {
            e.printStackTrace()
        } catch (e: NoSuchFieldException) {
            e.printStackTrace()
        }
        return orientation
    }

    fun rotateBitmap(src: String, bitmap: Bitmap): Bitmap {
        try {
            val orientation = getExifOrientation(src)
            if (orientation == 1) {
                return bitmap
            }
            val matrix = Matrix()
            when (orientation) {
                2 -> matrix.setScale(-1f, 1f)
                3 -> matrix.setRotate(180f)
                4 -> {
                    matrix.setRotate(180f)
                    matrix.postScale(-1f, 1f)
                }
                5 -> {
                    matrix.setRotate(90f)
                    matrix.postScale(-1f, 1f)
                }
                6 -> matrix.setRotate(90f)
                7 -> {
                    matrix.setRotate(-90f)
                    matrix.postScale(-1f, 1f)
                }
                8 -> matrix.setRotate(-90f)
                else -> return bitmap
            }
            return try {
                val oriented = Bitmap.createBitmap(
                    bitmap,
                    0,
                    0,
                    bitmap.width,
                    bitmap.height,
                    matrix,
                    true
                )
                bitmap.recycle()
                oriented
            } catch (e: OutOfMemoryError) {
                e.printStackTrace()
                bitmap
            }
        } catch (e: IOException) {
            e.printStackTrace()
        }
        return bitmap
    }

    fun imageToByteArray(image: Image): ByteArray? {
        var data: ByteArray? = null
        try {
            if (image.format == ImageFormat.JPEG) {
                val planes = image.planes
                val buffer = planes[0].buffer
                data = ByteArray(buffer.capacity())
                buffer[data]
                return data
            } else if (image.format == ImageFormat.YUV_420_888) {
                data = NV21toJPEG(
                    YUV420toNV21(image),
                    image.width, image.height, 100
                )
                //data = YUV420toNV21(image);
            }
        } catch (e: Exception) {
        }
        return data
    }

    fun NV21toJPEG(
        nv21: ByteArray?,
        width: Int,
        height: Int,
        quality: Int
    ): ByteArray {
        val out = ByteArrayOutputStream()
        val yuv = YuvImage(nv21, ImageFormat.NV21, width, height, null)
        yuv.compressToJpeg(Rect(0, 0, width, height), quality, out)
        return out.toByteArray()
    }

    fun YUV420toNV21(image: Image): ByteArray {
        val crop = image.cropRect
        val format = image.format
        val width = crop.width()
        val height = crop.height()
        val planes = image.planes
        val data =
            ByteArray(width * height * ImageFormat.getBitsPerPixel(format) / 8)
        val rowData = ByteArray(planes[0].rowStride)
        var channelOffset = 0
        var outputStride = 1
        for (i in planes.indices) {
            when (i) {
                0 -> {
                    channelOffset = 0
                    outputStride = 1
                }
                1 -> {
                    channelOffset = width * height + 1
                    outputStride = 2
                }
                2 -> {
                    channelOffset = width * height
                    outputStride = 2
                }
            }
            val buffer = planes[i].buffer
            val rowStride = planes[i].rowStride
            val pixelStride = planes[i].pixelStride
            val shift = if (i == 0) 0 else 1
            val w = width shr shift
            val h = height shr shift
            buffer.position(rowStride * (crop.top shr shift) + pixelStride * (crop.left shr shift))
            for (row in 0 until h) {
                var length: Int
                if (pixelStride == 1 && outputStride == 1) {
                    length = w
                    buffer[data, channelOffset, length]
                    channelOffset += length
                } else {
                    length = (w - 1) * pixelStride + 1
                    buffer[rowData, 0, length]
                    for (col in 0 until w) {
                        data[channelOffset] = rowData[col * pixelStride]
                        channelOffset += outputStride
                    }
                }
                if (row < h - 1) {
                    buffer.position(buffer.position() + rowStride - length)
                }
            }
        }
        return data
    }

    fun bitmapToByteArray(bitmap: Bitmap?): ByteArray? {
        bitmap ?: return null
        return try {
            val stream = ByteArrayOutputStream()
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream)
            val byteArray = stream.toByteArray()
            stream.close()
            byteArray
        } catch (e: Exception) {
            null
        }
    }

    fun rotateBitmapFlip(bitmap: Bitmap, degree: Float?, isFlip: Boolean): Bitmap {
        val matrix = Matrix()
        matrix.postRotate(degree!!)
        if (isFlip) matrix.postScale(-1f, 1f, bitmap.width / 2f, bitmap.height / 2f)
        return Bitmap.createBitmap(
            bitmap,
            0,
            0,
            bitmap.width,
            bitmap.height,
            matrix,
            true
        )
    }

    fun NV21toBitmap(byteArrayNV21: ByteArray?, width: Int, height: Int): Bitmap {
        val byteToNV21 = NV21toJPEG(byteArrayNV21, width, height, 100)
        val bitmap = BitmapFactory.decodeByteArray(byteToNV21, 0, byteToNV21.size)
        val matrix = Matrix()
        matrix.postRotate(90f)
        val bmRotation =
            Bitmap.createBitmap(bitmap, 0, 0, bitmap.width, bitmap.height, matrix, true)
        val bm: Bitmap
        bm = if (bmRotation.width >= bmRotation.height) {
            Bitmap.createBitmap(
                bmRotation,
                bmRotation.width / 2 + bmRotation.height / 2,
                0,
                bmRotation.height,
                bmRotation.height - 300
            )
        } else {
            val minus = bmRotation.width / 3.6
            Bitmap.createBitmap(
                bmRotation,
                0,
                bmRotation.height / 2 - bmRotation.width / 3,
                bmRotation.width,
                (bmRotation.width - minus).toInt()
            )
        }
        return bm
    }

    fun cropBitmap(bitmap: Bitmap): Bitmap? {
        return try {
            val matrix = Matrix()
            //            matrix.postRotate(-180f);
            val bmRotation = Bitmap.createBitmap(
                bitmap,
                0,
                0,
                bitmap.width,
                bitmap.height,
                matrix,
                true
            )
            val bm: Bitmap
            bm = if (bmRotation.width >= bmRotation.height) {
                Bitmap.createBitmap(
                    bmRotation,
                    bmRotation.width / 2 + bmRotation.height / 2,
                    0,
                    bmRotation.height,
                    bmRotation.height - 300
                )
            } else {
                val minus = bmRotation.width / 3.6
                Bitmap.createBitmap(
                    bmRotation,
                    0,
                    bmRotation.height / 2 - bmRotation.width / 2,
                    bmRotation.width,
                    (bmRotation.width - minus).toInt()
                )
            }
            bm
        } catch (e: Exception) {
            null
        }
    }

    private fun blurBitmap(ctx: Context?, image: Bitmap): Bitmap? {
        val width = (image.width * 1f).roundToInt()
        val height = (image.height * 1f).roundToInt()
        val inputBitmap = Bitmap.createScaledBitmap(image, width, height, false)
        val outputBitmap = Bitmap.createBitmap(inputBitmap)
        val rs: RenderScript = RenderScript.create(ctx)
        val theIntrinsic: ScriptIntrinsicBlur = ScriptIntrinsicBlur.create(rs, Element.U8_4(rs))
        val tmpIn: Allocation = Allocation.createFromBitmap(rs, inputBitmap)
        val tmpOut: Allocation = Allocation.createFromBitmap(rs, outputBitmap)
        theIntrinsic.setRadius(10f)
        theIntrinsic.setInput(tmpIn)
        theIntrinsic.forEach(tmpOut)
        tmpOut.copyTo(outputBitmap)
        return outputBitmap
    }

    fun getCropZoneFace(bitmap: Bitmap, rectFace: Rect): Bitmap{
        val rectCrop = Rect(0,0,bitmap.width,rectFace.centerY()*2)
        return Bitmap.createBitmap(bitmap,rectCrop.left,rectCrop.top,rectCrop.width(),rectCrop.height())
    }

    fun getCircularBitmap(bitmap: Bitmap): Bitmap? {
        val output = if (bitmap.width > bitmap.height) {
            Bitmap.createBitmap(bitmap.height, bitmap.height, Bitmap.Config.ARGB_8888)
        } else {
            Bitmap.createBitmap(bitmap.width, bitmap.width, Bitmap.Config.ARGB_8888)
        }
        val canvas = Canvas(output)
        val color = -0xbdbdbe
        val paint = Paint()
        val rect = Rect(0, 0, bitmap.width, bitmap.height)
        var r = 0f
        r = if (bitmap.width > bitmap.height) {
            bitmap.height / 2.toFloat()
        } else {
            bitmap.width / 2.toFloat()
        }
        paint.isAntiAlias = true
        canvas.drawARGB(0, 0, 0, 0)
        paint.color = color
        canvas.drawCircle(r, r, r, paint)
        paint.xfermode = PorterDuffXfermode(PorterDuff.Mode.SRC_IN)
        canvas.drawBitmap(bitmap, rect, rect, paint)
        return output
    }
}