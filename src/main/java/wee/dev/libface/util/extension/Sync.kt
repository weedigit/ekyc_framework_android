package wee.dev.libface.util.extension

import android.os.Handler
import android.os.Looper
import android.view.View
import java.util.concurrent.Executor
import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors

val HANDLER = Handler()

val UI_HANDLER: Handler = Handler(Looper.getMainLooper())

val UI_EXECUTOR: Executor = Executor { command -> UI_HANDLER.post(command) }

val IO_HANDLER: ExecutorService get() = Executors.newSingleThreadExecutor()

val IO_EXECUTOR: Executor get() = Executor { command -> IO_HANDLER.execute(command) }

val isOnUiThread: Boolean get() = Looper.myLooper() == Looper.getMainLooper()

fun uiThread(block: () -> Unit) {
    if (isOnUiThread) block()
    else post { block() }
}

fun ioThread(block: () -> Unit) {
    IO_HANDLER.execute(block)
}

fun postUI(block: () -> Unit) {
    UI_HANDLER.post { block() }
}

fun postUI(time: Long, block: () -> Unit) {
    UI_HANDLER.postDelayed({ block() }, time)
}

fun post(block: () -> Unit) {
    HANDLER.post { block() }
}

fun post(delay: Long, block: () -> Unit) {
    HANDLER.postDelayed({ block() }, delay)
}

fun show(vararg v : View){
    UI_HANDLER.post { for(view in v) view.visibility = View.VISIBLE }
}

fun hide(vararg v : View){
    UI_HANDLER.post { for(view in v) view.visibility = View.INVISIBLE }
}

fun gone(vararg v : View){
    UI_HANDLER.post { for(view in v) view.visibility = View.GONE }
}