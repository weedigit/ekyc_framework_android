package wee.dev.libface.base

import android.graphics.Bitmap
import android.graphics.Rect

interface OnVerifyFaceListener{
    fun onStart(isStart: Boolean,face: Bitmap?,rectFace: Rect?)
    fun onResult(faces: ArrayList<Bitmap?>, countFrontFace : Int)
    fun onFaceStatus(status: Int)
    fun atItem(item: PointData)
    fun onFrontFace(bitmap: Bitmap?,rectFace: Rect?)
    fun onFaceData(image: Bitmap?)
}

interface FrameStreamListener{
    fun onFrame(byteArray: ByteArray)
}

interface FaceActionPointListener {
    fun onActionAngle(item: Int, angle: Float)
}

interface CropObjectListener{
    fun onResult(bitmap: Bitmap)
}

interface VideoRecProcessListener{
    fun onDone(filePath: String)
}