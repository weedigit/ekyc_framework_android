package wee.dev.libface.base

import android.graphics.Point
import android.graphics.Rect
import com.google.firebase.ml.vision.face.FirebaseVisionFace

data class FaceResult(
    val Face: FirebaseVisionFace?,
    val Data: ByteArray?,
    val Blur: Int,
    val facePointData: FacePointData?
)

data class FacePointData(
        var faceRect: Rect,
        var RightEye: Point,
        var LeftEye: Point,
        var Nose: Point,
        var Rightmouth: Point,
        var Leftmouth: Point
)

data class PointData(
        var position: Int,
        var status: Boolean
)

data class LiveNessCheckData(
        val faceData: ByteArray?,
        val status: Boolean
)