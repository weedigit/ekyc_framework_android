package wee.dev.libface.camera

import android.util.Size
import com.google.firebase.ml.vision.common.FirebaseVisionImageMetadata
import com.google.firebase.ml.vision.face.FirebaseVisionFaceDetectorOptions
import kotlin.math.roundToInt

object CameraConfig{
    const val CAMERA_WIDTH = 1280

    const val CAMERA_HEIGHT = 720

    const val MAX_FRONT_COUNT = 1

    const val MAX_START_COUNT = 3

    const val MIN_FACE_SIZE = CAMERA_HEIGHT *0.35f

    const val MAX_FACE_SIZE = CAMERA_HEIGHT * 0.83f /*CAMERA_HEIGHT * 0.73f*/

    val FRONT_FACE_VALUE = -18f..18f/*-6f..6f*/

    var pSizeTrackingZone = Size((CAMERA_HEIGHT *0.20f).roundToInt(),(CAMERA_HEIGHT *0.20f).roundToInt())

    var pSizeStartZone =  Size((CAMERA_HEIGHT *0.10f).roundToInt(),(CAMERA_HEIGHT *0.10f).roundToInt())

    val MLKitFaceOption = FirebaseVisionFaceDetectorOptions.Builder()
        .setClassificationMode(FirebaseVisionFaceDetectorOptions.NO_CLASSIFICATIONS)
        .setLandmarkMode(FirebaseVisionFaceDetectorOptions.ALL_LANDMARKS)
        .setPerformanceMode(FirebaseVisionFaceDetectorOptions.FAST)
        .setContourMode(FirebaseVisionFaceDetectorOptions.NO_CONTOURS)
        .setMinFaceSize(0.5f)
        .build()

    val MLKitMetadata = FirebaseVisionImageMetadata.Builder()
        .setFormat(FirebaseVisionImageMetadata.IMAGE_FORMAT_NV21)
        .setWidth(CAMERA_WIDTH)
        .setHeight(CAMERA_HEIGHT)
        .setRotation(3)
        .build()

    var scaleX = 1.0f

    var scaleY = 1.0f

    var facing = CameraSource.CAMERA_FACING_FRONT

    var rotation = 3
}