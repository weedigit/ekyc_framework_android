package wee.dev.libface.control

import android.app.Activity
import android.content.Context
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.Rect
import android.util.AttributeSet
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import androidx.constraintlayout.widget.ConstraintLayout
import ekyc.Delegate
import ekyc.Ekyc
import kotlinx.android.synthetic.main.control_ekycface_layout.view.*
import wee.dev.libface.R
import wee.dev.libface.base.LiveNessCheckData
import wee.dev.libface.base.OnVerifyFaceListener
import wee.dev.libface.base.PointData
import wee.dev.libface.base.VideoRecProcessListener
import wee.dev.libface.util.BitmapUtils
import wee.dev.libface.util.FaceUtils
import wee.dev.libface.util.LanguageHelper
import wee.dev.libface.util.extension.gone
import wee.dev.libface.util.extension.postUI
import wee.dev.libface.util.extension.show

class EkycFaceView : ConstraintLayout, OnVerifyFaceListener, Delegate {

    private val cameraPermission = android.Manifest.permission.CAMERA

    private var permission = false

    private var isComplete = false

    private var isWaitItem = false

    private var isStarted = false

    private var isVideo = false

    private var userFrontFace: Bitmap? = null

    private var arrayBitmap = arrayListOf<Bitmap>()

    private var listener: EkycFaceViewCallBack? = null

    private var checkFaceControl: CheckFaceControl? = null

    constructor(context: Context) : super(context) {
        initView(context, null)
    }

    constructor(context: Context, attributeSet: AttributeSet?) : super(context, attributeSet) {
        initView(context, attributeSet)
    }

    constructor(context: Context, attributeSet: AttributeSet?, defStyleAttr: Int) : super(context, attributeSet, defStyleAttr) {
        initView(context, attributeSet)
    }

    private fun initView(context: Context, attributeSet: AttributeSet?) {
        LayoutInflater.from(context)
                .inflate(R.layout.control_ekycface_layout, this) as ConstraintLayout

        checkFaceControl = findViewById(R.id.ekycFaceControl_checkFaceControl)

    }

    /**
     * init EkycFaceView
     */
    fun initLibrary(l: EkycFaceViewCallBack, language: String) {
        isVideo = false
        LanguageHelper.initLang((context as Activity), language)
        permission =
                context.checkSelfPermission(cameraPermission) == PackageManager.PERMISSION_GRANTED
        if (!permission) return
        listener = l
        checkFaceControl?.setActivity((context as Activity), "")
        checkFaceControl?.initControl(this, false)
        checkFaceControl?.resumeControl()
        checkFaceControl?.startVerify()
    }

    fun onPauseLibrary() {
        checkFaceControl?.pauseControl()
    }

    fun onResumeLibrary() {
        if (permission) checkFaceControl?.resumeControl()
    }

    fun onDestroyLibrary() {
        checkFaceControl?.destroyControl()
    }

    fun changeLogoLibrary(logo: Int) {
        ekycFaceControl_checkFaceControl.checkFaceControlChangeLogo(logo)
    }

    fun onBackClickListener(listener: OnClickListener) {
        ekycFaceControl_checkFaceControl.checkFaceControlOnBack(OnClickListener {
            listener.onClick(it)
        })
    }

    /**
     * implement CheckFaceControl
     */
    override fun onStart(isStart: Boolean, face: Bitmap?, rectFace: Rect?) {
        if (isStarted) return
        isStarted = isStart
        if (isStart && face != null && rectFace != null) {
            userFrontFace = FaceUtils().flipBimtap(face)
            Ekyc.initLibrary(this, "", "")
            Ekyc.startLivenessCheck()
            isWaitItem = false
            nextDirection()
        }
    }

    override fun onResult(faces: ArrayList<Bitmap?>, countFrontFace: Int) {
        Log.e("EkycFaceView", "countFrameFace : $countFrontFace")
        arrayBitmap.clear()
        for (model in faces) {
            if (model != null) arrayBitmap.add(FaceUtils().flipBimtap(model))
        }
    }

    override fun onFaceStatus(status: Int) {
        if (status == CheckFaceControl.TYPE_FACE_OK) return
        if (isWaitItem && isStarted && !isComplete) isStarted = false
    }

    override fun atItem(item: PointData) {
        isWaitItem = false
        Ekyc.updateDirectionStatus(item.status)
    }

    private fun nextDirection() {
        if (isComplete) return
        if (isWaitItem) return
        Ekyc.getNextDirection()
    }

    override fun onFrontFace(bitmap: Bitmap?, rectFace: Rect?) {
        if (isWaitItem) return
        postUI(400) { nextDirection() }
    }

    override fun onFaceData(image: Bitmap?) {}

    /**
     * implement Ekyc
     */
    override fun nextDirection(p0: ByteArray?) {
        isWaitItem = true
        checkFaceControl?.waitItem(p0)
    }

    override fun finishLivenessCheck(p0: Boolean) {
        isWaitItem = false
        isComplete = true
        checkFaceControl?.isOn = false
        ekycFaceControl_checkFaceControl.goneActionBack()
        if (arrayBitmap.isNullOrEmpty()) {
            checkFaceControl?.finishVerifyAnim(userFrontFace)
        } else {
            checkFaceControl?.finishVerifyAnim(arrayBitmap[0])
        }
        if (!isVideo) {
            postUI(1000) {
                checkFaceControl?.loadingTextGuid()
                onResultEkycControl(p0, "")
            }
            return
        }
        if (isVideo) {
            checkFaceControl?.pauseRecVideo()
            checkFaceControl?.getVideoFilePath(object : VideoRecProcessListener {
                override fun onDone(filePath: String) {
                    postUI(1000) {
                        checkFaceControl?.loadingTextGuid()
                        onResultEkycControl(p0, filePath)
                    }
                }
            })
        }
    }

    override fun timeoutDirection(p0: ByteArray?) {
        isWaitItem = false
        if (checkFaceControl?.isStart == true) nextDirection() else isStarted = false
    }

    override fun currentDirection(p0: ByteArray?) {}

    private fun onResultEkycControl(isLiveness: Boolean, filePath: String) {
        if (arrayBitmap.isNullOrEmpty()) {
            val data = LiveNessCheckData(BitmapUtils.bitmapToByteArray(userFrontFace), isLiveness)
            listener?.onComplete(data)
            return
        }
        if (arrayBitmap.size < 2) {
            val data = LiveNessCheckData(BitmapUtils.bitmapToByteArray(userFrontFace), isLiveness)
            listener?.onComplete(data)
        } else {
            val index = arrayBitmap.size / 2
            val data = LiveNessCheckData(BitmapUtils.bitmapToByteArray(arrayBitmap[index]), isLiveness)
            listener?.onComplete(data)
        }
    }

    /**
     * EkycFaceView callback
     */
    interface EkycFaceViewCallBack {
        fun onComplete(data: LiveNessCheckData)
    }

}