package wee.dev.libface.control

import android.app.Activity
import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import androidx.constraintlayout.widget.ConstraintLayout
import kotlinx.android.synthetic.main.permission_view.view.*
import wee.dev.libface.R
import wee.dev.libface.util.LanguageHelper

class PermissionView : ConstraintLayout {

    constructor(context: Context) : super(context) {
        initView(context, null)
    }

    constructor(context: Context, attributeSet: AttributeSet?) : super(context, attributeSet) {
        initView(context, attributeSet)
    }

    constructor(context: Context, attributeSet: AttributeSet?, defStyleAttr: Int) : super(context, attributeSet, defStyleAttr) {
        initView(context, attributeSet)
    }

    private fun initView(context: Context, attributeSet: AttributeSet?) {

        LayoutInflater.from(context).inflate(R.layout.permission_view, this)

        permissionView_faceControl.goneLottiePoint()

    }

    fun initLanguage(language: String) {

        LanguageHelper.initLang((context as Activity), language)

        permissionView_title.text = LanguageHelper.getStringLanguage("permission_title")

        permissionView_labelBlur.text = LanguageHelper.getStringLanguage("permission_blur")

        permissionView_labelContent.text = LanguageHelper.getStringLanguage("permission_content")

        permissionView_actionPermission.text = LanguageHelper.getStringLanguage("permission_action_turn_on")

    }

    fun changeLogo(logo: Int) {

        permissionView_logo.setImageResource(logo)

    }

    fun onBackClickListener(listener: OnClickListener) {

        permissionView_actionBack.setOnClickListener { listener.onClick(it) }

    }

    fun onClickTurnOnCamera(listener: OnClickListener) {

        permissionView_actionPermission.setOnClickListener { listener.onClick(it) }

    }

}