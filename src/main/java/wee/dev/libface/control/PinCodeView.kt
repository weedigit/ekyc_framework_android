package wee.dev.libface.control

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.text.Editable
import android.text.TextWatcher
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.WindowManager
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.text.HtmlCompat
import kotlinx.android.synthetic.main.pin_code_view.view.*
import wee.dev.libface.R
import wee.dev.libface.util.LanguageHelper
import wee.dev.libface.util.extension.*


class PinCodeView : ConstraintLayout {

    companion object {

        const val PIN_CREATE = "PIN_CREATE"

        const val PIN_VERIFY = "PIN_VERIFY"

    }

    private var isVerifyPin = false

    private var inputPin = ""

    private var typePin = ""

    private var isLock = false

    private var listener: PinCodeCallBack? = null

    constructor(context: Context) : super(context) {
        initView(context, null)
    }

    constructor(context: Context, attributeSet: AttributeSet?) : super(context, attributeSet) {
        initView(context, attributeSet)
    }

    constructor(context: Context, attributeSet: AttributeSet?, defStyleAttr: Int) : super(context, attributeSet, defStyleAttr) {
        initView(context, attributeSet)
    }

    @SuppressLint("StringFormatInvalid")
    private fun initView(context: Context, attributeSet: AttributeSet?) {

        LayoutInflater.from(context).inflate(R.layout.pin_code_view, this)

        (context as Activity).window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE or WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE)

        pinCodeView_dotView1.changeBackgroundDot(item1 = false, item2 = false, item3 = false, item4 = false, item5 = false, item6 = false)

        isLock = false
        isVerifyPin = false

        inputPin = ""

        gone(pinCodeView_loading)

        pinCodeView_loading.clearAnimation()

        pinCodeView_dotView1.setOnClickListener { resumePinCode() }

        pinCodeView_input.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                if (isLock) return

                bindViewDot(s.toString())

                if (pinCodeView_labelError.isShown) hide(pinCodeView_labelError)
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {}

        })
    }

    private fun bindViewDot(pin: String) {

        when (pin.length) {
            0 -> pinCodeView_dotView1.changeBackgroundDot(item1 = false, item2 = false, item3 = false, item4 = false, item5 = false, item6 = false)
            1 -> pinCodeView_dotView1.changeBackgroundDot(item1 = true, item2 = false, item3 = false, item4 = false, item5 = false, item6 = false)
            2 -> pinCodeView_dotView1.changeBackgroundDot(item1 = true, item2 = true, item3 = false, item4 = false, item5 = false, item6 = false)
            3 -> pinCodeView_dotView1.changeBackgroundDot(item1 = true, item2 = true, item3 = true, item4 = false, item5 = false, item6 = false)
            4 -> pinCodeView_dotView1.changeBackgroundDot(item1 = true, item2 = true, item3 = true, item4 = true, item5 = false, item6 = false)
            5 -> pinCodeView_dotView1.changeBackgroundDot(item1 = true, item2 = true, item3 = true, item4 = true, item5 = true, item6 = false)
            6 -> {
                pinCodeView_dotView1.changeBackgroundDot(item1 = true, item2 = true, item3 = true, item4 = true, item5 = true, item6 = true)

                if (typePin != PIN_CREATE) {
                    isLock = true

                    listener?.onResult(pin)

                    show(pinCodeView_loading)

                    pinCodeView_loading.playAnimation()

                    return
                }

                if (!isVerifyPin) {
                    inputPin = pin

                    changeViewConfirm()
                } else {
                    isLock = true

                    if (pin == inputPin) {
                        listener?.onResult(inputPin)

                        show(pinCodeView_loading)

                        pinCodeView_loading.playAnimation()
                    } else {
                        show(pinCodeView_labelError)

                        postUI(400) {
                            pinCodeView_input.setText("")

                            isLock = false

                            pinCodeView_dotView1.changeBackgroundDot(item1 = false, item2 = false, item3 = false, item4 = false, item5 = false, item6 = false)
                        }

                    }
                }
            }
        }
    }

    private fun changeViewConfirm() {

        pausePinCode()

        pinCodeView_dotView1.startAnimation(fadeOutAnim())

        postUI(200) {

            resumePinCode()

            isVerifyPin = true

            pinCodeView_labelTitle.text = LanguageHelper.getStringLanguage("pin_code_title_confirm")

            pinCodeView_input.setText("")

            pinCodeView_dotView1.changeBackgroundDot(item1 = false, item2 = false, item3 = false, item4 = false, item5 = false, item6 = false)

        }
    }

    fun changeLogo(logo: Int) {
        pinCodeView_logo.setImageResource(logo)
    }

    fun onBackClickListener(listener: OnClickListener) {
        pinCodeView_actionBack.setOnClickListener {
            if (typePin == PIN_CREATE) {
                if (isVerifyPin) {
                    resetPinCode()

                    hide(pinCodeView_labelError)
                    return@setOnClickListener
                }

                listener.onClick(it)
            } else {
                listener.onClick(it)
            }
        }
    }

    fun resetPinCode() {
        inputPin = ""

        isLock = false
        isVerifyPin = false

        pausePinCode()

        gone(pinCodeView_loading)
        pinCodeView_loading.pauseAnimation()

        pinCodeView_input.setText("")

        pinCodeView_dotView1.startAnimation(fadeOutAnim())
        pinCodeView_dotView1.changeBackgroundDot(item1 = false, item2 = false, item3 = false, item4 = false, item5 = false, item6 = false)

        if (typePin == PIN_CREATE) {
            pinCodeView_labelTitle.text = LanguageHelper.getStringLanguage("pin_code_title")

            pinCodeView_labelContent.text = HtmlCompat.fromHtml(LanguageHelper.getStringLanguage("pin_code_content"), HtmlCompat.FROM_HTML_MODE_LEGACY)
        } else {
            pinCodeView_labelTitle.text = LanguageHelper.getStringLanguage("pin_code_title_verify")

            pinCodeView_labelContent.text = LanguageHelper.getStringLanguage("pin_code_content_verify")
        }

        resumePinCode()
    }

    fun resumePinCode() {
        postUI(150) {
            pinCodeView_input.requestFocus()

            pinCodeView_input.showKeyboard()
        }
    }

    fun pausePinCode() {
        pinCodeView_input.clearFocus()

        pinCodeView_input.hideKeyboard()
    }

    fun initPinCodeCallBack(type: String, language: String, l: PinCodeCallBack) {
        typePin = type

        listener = l

        LanguageHelper.initLang((context as Activity), language)

        if (type == PIN_CREATE) {
            pinCodeView_labelTitle.text = LanguageHelper.getStringLanguage("pin_code_title")

            pinCodeView_labelContent.text = HtmlCompat.fromHtml(LanguageHelper.getStringLanguage("pin_code_content"), HtmlCompat.FROM_HTML_MODE_LEGACY)

            pinCodeView_labelError.text = LanguageHelper.getStringLanguage("pin_code_create_error")
        } else {
            pinCodeView_labelTitle.text = LanguageHelper.getStringLanguage("pin_code_title_verify")

            pinCodeView_labelContent.text = LanguageHelper.getStringLanguage("pin_code_content_verify")
        }
    }

    interface PinCodeCallBack {
        fun onResult(pin: String)
    }

}