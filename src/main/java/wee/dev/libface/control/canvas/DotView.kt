package wee.dev.libface.control.canvas

import android.content.Context
import android.content.res.ColorStateList
import android.util.AttributeSet
import android.view.LayoutInflater
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import kotlinx.android.synthetic.main.dot_view.view.*
import wee.dev.libface.R

class DotView : ConstraintLayout {

    constructor(context: Context) : super(context) {
        initView(context, null)
    }

    constructor(context: Context, attributeSet: AttributeSet?) : super(context, attributeSet) {
        initView(context, attributeSet)
    }

    constructor(context: Context, attributeSet: AttributeSet?, defStyleAttr: Int) : super(context, attributeSet, defStyleAttr) {
        initView(context, attributeSet)
    }

    private fun initView(context: Context, attributeSet: AttributeSet?) {
        LayoutInflater.from(context).inflate(R.layout.dot_view, this)
    }

    fun changeBackgroundDot(
            item1: Boolean,
            item2: Boolean,
            item3: Boolean,
            item4: Boolean,
            item5: Boolean,
            item6: Boolean
    ) {
        val colorUnSelect = ContextCompat.getColor(context, R.color.gray2)

        val colorSelect = ContextCompat.getColor(context, R.color.black)

        dotView_dot1.backgroundTintList =
                if (item1) ColorStateList.valueOf(colorSelect) else ColorStateList.valueOf(colorUnSelect)

        dotView_dot2.backgroundTintList =
                if (item2) ColorStateList.valueOf(colorSelect) else ColorStateList.valueOf(colorUnSelect)

        dotView_dot3.backgroundTintList =
                if (item3) ColorStateList.valueOf(colorSelect) else ColorStateList.valueOf(colorUnSelect)

        dotView_dot4.backgroundTintList =
                if (item4) ColorStateList.valueOf(colorSelect) else ColorStateList.valueOf(colorUnSelect)

        dotView_dot5.backgroundTintList =
                if (item5) ColorStateList.valueOf(colorSelect) else ColorStateList.valueOf(colorUnSelect)

        dotView_dot6.backgroundTintList =
                if (item6) ColorStateList.valueOf(colorSelect) else ColorStateList.valueOf(colorUnSelect)

    }

}