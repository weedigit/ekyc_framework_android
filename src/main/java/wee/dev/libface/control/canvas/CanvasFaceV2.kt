package wee.dev.libface.control.canvas

import android.animation.Animator
import android.animation.PropertyValuesHolder
import android.animation.ValueAnimator
import android.app.Activity
import android.content.Context
import android.graphics.Bitmap
import android.media.MediaPlayer
import android.os.Handler
import android.os.HandlerThread
import android.util.AttributeSet
import android.util.DisplayMetrics
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.animation.AccelerateDecelerateInterpolator
import androidx.constraintlayout.widget.ConstraintLayout
import kotlinx.android.synthetic.main.canvas_face_v2.view.*
import wee.dev.libface.R
import wee.dev.libface.util.LanguageHelper
import wee.dev.libface.util.ScreenUtils
import wee.dev.libface.util.ScreenUtils.gone
import wee.dev.libface.util.extension.show


class CanvasFaceV2 : ConstraintLayout {

    var listener: CanvasFaceListener? = null

    var canvas: RoundedV2? = null

    var canvasPoint: PointV2? = null

    private var isSuccessFace = false

    private val handlerMain = Handler(context.mainLooper)

    private var faceHandler: Handler? = null

    private var lineHandler: Handler? = null

    private var completeHandler: Handler? = null

    private var accessHandler: Handler? = null

    private var canvasFaceV2Thread = HandlerThread("canvasFaceV2Thread")

    private var canvasLineThread = HandlerThread("canvasLineThread")

    private var canvasCompleteThread = HandlerThread("canvasCompleteThread")

    private var canvasAccessThread = HandlerThread("canvasAccessThread")

    private var activity: Activity? = null

    private var mAudio: MediaPlayer? = null

    private var isPauseJson = false

    private var countRepeat = 0

    constructor(context: Context?) : super(context) {
        initView(context, null)
    }

    constructor(context: Context, attributes: AttributeSet?) : super(context, attributes) {
        initView(context, attributes)
    }

    private fun initView(context: Context?, attributes: AttributeSet?) {
        LayoutInflater.from(context).inflate(R.layout.canvas_face_v2, this)

        isSuccessFace = false

        canvas = findViewById(R.id.faceCanvasV2)

        canvasPoint = findViewById(R.id.faceCanvasPointV2)

        canvas?.number = 1f

        canvasPoint?.scaleWith = 1f

        canvasPoint?.scaleCheck = 0f

        faceCanvasPointV2.setLayerType(View.LAYER_TYPE_HARDWARE, null)

        canvasFaceV2Thread.start()

        canvasCompleteThread.start()

        canvasLineThread.start()

        canvasAccessThread.start()

        mAudio = MediaPlayer.create(context, R.raw.ting)

        checkResolution()

        faceCanvasImgPoint.addAnimatorListener(object : Animator.AnimatorListener {

            override fun onAnimationRepeat(animation: Animator?) {

                countRepeat++

                if (isPauseJson && countRepeat % 2 == 0) {
                    faceCanvasImgPoint.pauseAnimation()

                    Log.e("faceCanvasImgPoint", "pause onAnimationRepeat $countRepeat")
                }

                Log.e("faceCanvasImgPoint", "onAnimationRepeat $countRepeat")
            }

            override fun onAnimationEnd(animation: Animator?) {
                Log.e("faceCanvasImgPoint", "onAnimationEnd")
            }

            override fun onAnimationCancel(animation: Animator?) {
                Log.e("faceCanvasImgPoint", "onAnimationCancel")
            }

            override fun onAnimationStart(animation: Animator?) {
                countRepeat = 0

                Log.e("faceCanvasImgPoint", "onAnimationStart")
            }

        })
    }

    private fun checkResolution() {
        val displayMetrics = DisplayMetrics()
        (context as Activity).windowManager.defaultDisplay.getMetrics(displayMetrics)
        val height = displayMetrics.heightPixels
        val width = displayMetrics.widthPixels
        if (height < 1920 && width < 1080) {
            canvasPoint?.sizeLineCheck = 60f
            canvasPoint?.sizeLine = 20f
        }
    }

    fun setInchShadow(activity: Activity) {
        this.activity = activity
        val inch = getInchScreen(activity)
        when {
            inch >= 5.6 && inch < 7f -> {
                guidlineShadown.setGuidelinePercent(0f)
                guidlineShadown2.setGuidelinePercent(0.77f)
            }
            inch >= 7 -> {
                guidlineShadown.setGuidelinePercent(-0.2f)
                guidlineShadown2.setGuidelinePercent(0.98f)
            }
            else -> {
                gone(canvasFaceShadown)
                guidlineShadown.setGuidelinePercent(0.031f)
                guidlineShadown2.setGuidelinePercent(0.74f)
            }
        }
    }

    private fun getInchScreen(activity: Activity): Double {
        val dm = DisplayMetrics()
        activity.windowManager.defaultDisplay.getMetrics(dm)
        val width = dm.widthPixels
        val height = dm.heightPixels
        val wi = width / dm.xdpi
        val hi = height / dm.ydpi
        val x = Math.pow(wi.toDouble(), 2.toDouble())
        val y = Math.pow(hi.toDouble(), 2.toDouble())
        return Math.sqrt(x + y)
    }

    fun initListener(l: CanvasFaceListener) {
        listener = l
    }

    fun showLine() {
        canvasPoint?.scaleCheck = 1f
    }

    fun hideAnim() {
        canvasPoint?.scaleCheck = 0f
    }

    fun topLeft() {
        handlerAnimationLine(40f, 200f)
    }

    fun centerLeft() {
        handlerAnimationLine(30f, 165f)
    }

    fun botLeft() {
        handlerAnimationLine(40f, 120f)
    }

    fun top() {
        handlerAnimationLine(40f, 250f)
    }

    fun bot() {
        handlerAnimationLine(40f, 70f)
    }

    fun topRight() {
        handlerAnimationLine(40f, 300f)
    }

    fun centerRight() {
        handlerAnimationLine(30f, 345f)
    }

    fun botRight() {
        handlerAnimationLine(40f, 20f)
    }

    private fun handlerAnimationLine(sweep: Float, start: Float) {
        lineHandler?.post {
            val numberFirst = PointV2.getStartAngle
            var number = numberFirst
            var isCheckReturn = false
            var isCheckBigger = false
            faceHandler?.post(object : Runnable {
                override fun run() {
                    canvasPoint?.sweepAngle = sweep
                    canvasPoint?.startAngle = number + 5f
                    number += 5f
                    if (number >= 360) {
                        number = 0f
                        isCheckBigger = true
                    }
                    if (isCheckBigger && number >= numberFirst) {
                        isCheckReturn = true
                    }
                    if (isCheckReturn && number == start) {
                        faceHandler?.removeCallbacks(this)
                        listener?.onAnimationLineComplete()
                        return
                    }
                    faceHandler?.postDelayed(this, 2.5.toLong())
                }
            })
        }
    }

    private fun playAudio() {
        try {
            mAudio?.start()
        } catch (e: Exception) {
            Log.e("playAudio", "${e.message}")
        }
    }

    fun successLine() {
        canvasPoint?.colorCheck = R.color.green
        playAudio()
        val nb = 0.002f
        canvasPoint?.scaleCheck = 1f
        var isAnimMinus = false
        var max = 1f
        var min = 0.8f
        faceHandler?.postDelayed(object : Runnable {
            override fun run() {
                if (!isAnimMinus) {
                    canvasPoint?.scaleCheck = max - nb
                    max -= nb
                    if (max <= 0.8f) {
                        isAnimMinus = true
                    }
                } else {
                    canvasPoint?.scaleCheck = min + nb
                    min += nb
                    if (min >= 1f) {
                        canvasPoint?.colorCheck = R.color.blue
                        faceHandler?.removeCallbacks(this)
                        listener?.onSuccessLineComplete()
                        return
                    }
                }
                faceHandler?.postDelayed(this, 3)
            }
        }, 100)
    }


    fun failItem() {
        canvasPoint?.colorCheck = R.color.red
        val nb = 0.002f
        canvasPoint?.scaleCheck = 1f
        var isAnimMinus = false
        var max = 1f
        var min = 0.8f
        faceHandler?.postDelayed(object : Runnable {
            override fun run() {
                if (!isAnimMinus) {
                    canvasPoint?.scaleCheck = max - nb
                    max -= nb
                    if (max <= 0.8f) {
                        isAnimMinus = true
                    }
                } else {
                    canvasPoint?.scaleCheck = min + nb
                    min += nb
                    if (min >= 1f) {
                        canvasPoint?.colorCheck = R.color.blue
                        faceHandler?.removeCallbacks(this)
                        listener?.onSuccessLineComplete()
                        return
                    }
                }
                faceHandler?.postDelayed(this, 4)
            }
        }, 100)
    }

    fun resetAfterComplete() {
        isSuccessFace = false
        handlerMain.post {
            canvasFaceShadown?.visibility = View.VISIBLE
            canvasFaceStatusFace?.visibility = View.VISIBLE
            faceCanvasImgPoint?.visibility = View.VISIBLE
        }
        canvas?.reset()
        canvas?.number = 1f
        canvasPoint?.scaleWith = 1f
        canvasPoint?.scaleCheck = 0f
        canvasFaceLottieLoading.visibility = View.GONE
        canvasFaceFullFace.visibility = View.GONE
    }

    fun completeLiveness(bm: Bitmap?) {
        isSuccessFace = true
        handlerMain.post {
            canvasFaceShadown.visibility = View.GONE
            canvasFaceStatusFace.visibility = View.GONE
            faceCanvasImgPoint.visibility = View.GONE
            canvasFaceFullFace.visibility = View.VISIBLE
            canvasFaceFullFace.setImageBitmap(bm)
        }
        canvas?.number = 1f
        canvasPoint?.scaleWith = 1f
        canvasPoint?.scaleCheck = 0f
        val propertyX: PropertyValuesHolder = PropertyValuesHolder.ofFloat("COMPLETE", 1f, 0.62f)
        val propertyY: PropertyValuesHolder = PropertyValuesHolder.ofFloat("COMPLETEY", 1f, 0.53f)
        val animator = ValueAnimator()
        animator.setValues(propertyX, propertyY)
        animator.duration = 800
        animator.interpolator = AccelerateDecelerateInterpolator()
        animator.removeAllUpdateListeners()
        animator.addUpdateListener { value ->
            val min = value.getAnimatedValue("COMPLETE") as Float
            val minY = value.getAnimatedValue("COMPLETEY") as Float
            canvasFaceFullFace.scaleX = minY
            canvasFaceFullFace.scaleY = minY
            canvas?.number = min
            canvasPoint?.scaleWith = min
            if (min <= 0.62f) {
                successLiveNess()
            }
        }
        animator.start()
    }

    private fun successLiveNess() {
        handlerMain.post {
            ScreenUtils.show(canvasFaceLottieLoading)
            canvasFaceLottieLoading.playAnimation()
        }
    }

    fun goneLottiePoint() {
        gone(faceCanvasImgPoint)
    }

    fun setImgPoint(number: Int) {
        handlerMain.post {
            show(faceCanvasImgPoint)
            when (number) {
                1 -> faceCanvasImgPoint.setAnimation("top_left.json")
                2 -> faceCanvasImgPoint.setAnimation("center_left.json")
                3 -> faceCanvasImgPoint.setAnimation("bottom_left.json")
                4 -> faceCanvasImgPoint.setAnimation("top_right.json")
                5 -> faceCanvasImgPoint.setAnimation("center_right.json")
                6 -> faceCanvasImgPoint.setAnimation("bottom_right.json")
                7 -> faceCanvasImgPoint.setAnimation("top.json")
                8 -> faceCanvasImgPoint.setAnimation("bottom.json")
            }
            if (number in 1..8) {
                faceCanvasImgPoint.playAnimation()
                isPauseJson = false
            } else {
                isPauseJson = true
            }

        }
    }

    fun setBlurView(number: Int?) {
        if (isSuccessFace) return
        handlerMain.post {
            if (number == null) {
                canvasFaceStatusFace.visibility = View.GONE
                canvasFaceBlur.visibility = View.GONE
            } else {
                canvasFaceStatusFace.visibility = View.VISIBLE
                canvasFaceBlur.visibility = View.VISIBLE
                isPauseJson = true
                when (number) {
                    0 -> canvasFaceStatusFace.text = LanguageHelper.getStringLanguage("blur_view_0")
                    1 -> canvasFaceStatusFace.text = LanguageHelper.getStringLanguage("blur_view_1")
                    2 -> canvasFaceStatusFace.text = LanguageHelper.getStringLanguage("blur_view_2")
                    3 -> canvasFaceStatusFace.text = LanguageHelper.getStringLanguage("blur_view_3")
                    4 -> canvasFaceStatusFace.text = LanguageHelper.getStringLanguage("blur_view_4")
                    5 -> canvasFaceStatusFace.text = LanguageHelper.getStringLanguage("blur_view_5")
                    6 -> canvasFaceStatusFace.text = LanguageHelper.getStringLanguage("blur_view_6")
                    7 -> canvasFaceStatusFace.text = LanguageHelper.getStringLanguage("blur_view_7")
                    8 -> canvasFaceStatusFace.text = LanguageHelper.getStringLanguage("blur_view_8")
                }
            }

        }
    }

    fun startHandlerThread() {
        if (faceHandler == null) faceHandler = Handler(canvasFaceV2Thread.looper)
        if (completeHandler == null) completeHandler = Handler(canvasCompleteThread.looper)
        if (lineHandler == null) lineHandler = Handler(canvasLineThread.looper)
        if (accessHandler == null) accessHandler = Handler(canvasAccessThread.looper)
    }

    fun onDestroy() {
        mAudio?.release()
        canvasFaceV2Thread.quitSafely()
        canvasCompleteThread.quitSafely()
        canvasLineThread.quitSafely()
        canvasAccessThread.quitSafely()
        try {
            canvasFaceV2Thread.join()
            canvasCompleteThread.join()
            canvasLineThread.join()
            canvasAccessThread.join()
        } catch (e: Exception) {
            Log.e("CanvasFaceV2", "${e.message}")
        }
    }

    interface CanvasFaceListener {
        fun onAnimationLineComplete()
        fun onSuccessLineComplete()

    }

}