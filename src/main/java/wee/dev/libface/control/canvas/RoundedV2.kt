package wee.dev.libface.control.canvas

import android.content.Context
import android.graphics.*
import android.os.Handler
import android.os.Looper
import android.util.AttributeSet
import android.view.View
import androidx.core.graphics.toRect
import wee.dev.libface.R
import wee.dev.libface.util.ScreenUtils

class RoundedV2 @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : View(context, attrs, defStyleAttr) {

    private val handlerMain = Handler(context.mainLooper)

    private var drawFace: Bitmap? = null
        set(scale) {
            field = scale
            handlerMain.post { invalidate() }
        }

    var number = 0f
        set(value) {
            field = value
            handlerMain.post { invalidate() }
        }

    fun reset(){
        drawFace = null
        handlerMain.post { invalidate() }
    }

    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)
        val path = Path()
        path.reset()
        path.addCircle((width *0.5f), (height * 0.385f), number * (width / 2f) - (width*0.1f), Path.Direction.CW)
        canvas.clipPath(path,Region.Op.DIFFERENCE)
        canvas.drawColor(context.getColor(R.color.white))
    }
}