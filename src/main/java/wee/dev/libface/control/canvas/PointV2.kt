@file:Suppress("DEPRECATION")

package wee.dev.libface.control.canvas

import android.content.Context
import android.graphics.Canvas
import android.graphics.Path
import android.graphics.Region
import android.os.Handler
import android.util.AttributeSet
import android.view.View
import wee.dev.libface.R
import wee.dev.libface.util.ScreenUtils

class PointV2 @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : View(context, attrs, defStyleAttr) {

    private var mainHandler = Handler(context.mainLooper)

    companion object {
        var getStartAngle = 0f
        var getSweepAngle = 0f
    }

    var scaleCheck = 0f
        set(scale) {
            field = scale
            mainHandler.post {  invalidate()  }
        }

    var colorCheck = R.color.blue
        set(value) {
            field = value
            mainHandler.post { invalidate()  }
        }

    var sizeLineCheck = 90f
        set(value) {
            field = value
            mainHandler.post {  invalidate()  }
        }

    var sizeLine = 30f
        set(value) {
            field = value
            mainHandler.post {  invalidate()  }
        }

    var startAngle = 20f
        set(scale) {
            field = scale
            mainHandler.post {  invalidate()  }
        }

    var sweepAngle = 40f
        set(scale) {
            field = scale
            mainHandler.post {  invalidate()  }
        }

    var scaleWith = 70f
        set(value) {
            field = value
            mainHandler.post {  invalidate()  }
        }

    private var colorLine = R.color.colorBorderLineBackground
        set(value) {
            field = value
            mainHandler.post {  invalidate()  }
        }

    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)

        getStartAngle = startAngle
        getSweepAngle = sweepAngle

        val centerX = (width / 2).toFloat()
        val centerY = (height / 2.6).toFloat()

        val path = Path()
        path.reset()
        path.addCircle(
            (width / 2).toFloat(),
            (height / 2.6).toFloat(),
            (width / 2f) - (width / 10f),
            Path.Direction.CW
        )
        canvas.clipPath(path, Region.Op.DIFFERENCE)

        val radiusLineCheck = scaleCheck * ((width / 2).toFloat()) - (width / 10f) + 10f
        val painLineCheck = ScreenUtils.CreatePaint(context, colorCheck, sizeLineCheck)
        val ovalLineCheck = ScreenUtils.CreateOval(painLineCheck, centerX, centerY, radiusLineCheck)

        canvas.drawArc(ovalLineCheck, startAngle, sweepAngle, false, painLineCheck)

        val radiusLine = scaleWith * (width / 2 - ((width / 10f) - 10f))
        val paintLine = ScreenUtils.CreatePaint(context, colorLine, sizeLine)
        val ovalLine = ScreenUtils.CreateOval(paintLine, centerX, centerY, radiusLine)
        canvas.drawArc(ovalLine, 0f, 360f, false, paintLine)
    }

}