package wee.dev.libface.control.canvas

import android.content.Context
import android.graphics.Canvas
import android.graphics.Paint
import android.util.AttributeSet
import android.view.View
import wee.dev.libface.R


class BlurV2 @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : View(context, attrs, defStyleAttr) {


    override fun onDraw(canvas: Canvas) {

        val paint = Paint()
        paint.color = context.getColor(R.color.colorBlurCamera)
        canvas.drawCircle(
            (width / 2).toFloat(),
            (height / 2.6).toFloat(),
            (width / 2f) - (width / 10f),
            paint
        )
    }
}