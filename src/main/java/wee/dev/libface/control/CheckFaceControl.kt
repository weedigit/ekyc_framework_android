package wee.dev.libface.control

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.content.pm.PackageManager.PERMISSION_GRANTED
import android.graphics.Bitmap
import android.graphics.Point
import android.os.*
import android.util.AttributeSet
import android.util.Log
import android.view.View
import androidx.constraintlayout.widget.ConstraintLayout
import com.google.android.gms.vision.face.Landmark
import com.google.firebase.FirebaseApp
import com.google.firebase.ml.vision.FirebaseVision.getInstance
import com.google.firebase.ml.vision.common.FirebaseVisionImage
import com.google.firebase.ml.vision.face.FirebaseVisionFace
import com.google.firebase.ml.vision.face.FirebaseVisionFaceDetector
import com.google.gson.Gson
import kotlinx.android.synthetic.main.control_livenesscheckface_layout.view.*
import wee.dev.libface.R
import wee.dev.libface.base.*
import wee.dev.libface.camera.CameraConfig
import wee.dev.libface.camera.CameraConfig.MAX_FRONT_COUNT
import wee.dev.libface.camera.CameraConfig.MAX_START_COUNT
import wee.dev.libface.camera.CameraSource
import wee.dev.libface.camera.GraphicOverlay
import wee.dev.libface.camera.PreviewFullScreen
import wee.dev.libface.control.canvas.CanvasFaceV2
import wee.dev.libface.util.*
import wee.dev.libface.util.ScreenUtils.gone
import wee.dev.libface.util.ScreenUtils.show
import wee.dev.libface.util.extension.postUI
import java.io.IOException
import kotlin.math.*


class CheckFaceControl : ConstraintLayout, CanvasFaceV2.CanvasFaceListener, Sensor.SensorListener {

    companion object {
        const val TAG = "VerifyFaceControl"
        const val TYPE_FACE_OK = 0
        const val TYPE_FACE_NO = 1
        const val TYPE_FACE_OUT_ZONE = 2
        const val TYPE_FACE_OUT_SMALL_SIZE = 3
        const val TYPE_FACE_OUT_BIG_SIZE = 4
        const val TYPE_FACE_WRONG_FRONT = 5
        const val TYPE_FACE_WRONG_LOW = 6
        const val TYPE_FACE_WRONG_HIGH = 7
        const val TYPE_NO_HOLD_2_HANDS = 8
        const val SHOW_MESS_COUNT = 8
    }

    private var mRootView: View? = null
    var isOn = false

    private var numberLiveNess = 0

    //---
    private var mCameraSource: CameraSource? = null
    private var mPreviewFullScreen: PreviewFullScreen? = null
    private var mGraphicOverlay: GraphicOverlay? = null
    private var mTrackingFaceControl: TrackingFaceControlV2? = null
    private var sensor: Sensor? = null

    //---
    private var mVerifyFaceHandler: Handler? = null
    private var mVerifyFaceThread: HandlerThread? = null
    private var mModelFaceHandler: Handler? = null
    private var mModelFaceThread: HandlerThread? = null

    private var isStartTimeOut = false
    private var leftAnim = false
    private var rightAnim = false
    private var numberTimeOut = 4
    private var countFrontFace = 0
    private var arrayBitmap = arrayListOf<Bitmap?>()

    private var mMainHandler: Handler? = null
    private var isFaceProcessing = false
    private var mCurFaceID = -1
    private var mFaceDetector: FirebaseVisionFaceDetector? = null

    var onVerifyFaceListener: OnVerifyFaceListener? = null
    var isStart = false

    private var mStartCount = MAX_START_COUNT
    private var mFrontCount = MAX_FRONT_COUNT
    private var mFaceUtils = FaceUtils()
    private var mWaitItem = 0
    private var mCurItem = -1
    private var mWrongCount = 0
    private var mAngleZ = 0f
    private var isSleep = false
    private var mVib: Vibrator? = null
    private var mFrontFace: FirebaseVisionFace? = null
    private var mFaceStatus = TYPE_FACE_NO
    private val mVideoUtils = VideoUtils(context)
    var isVideoRec = false
    private var mFaceDegree = 0f
    private var mNowItem = 0
    private var isItemSuccess = false
    private var showMessCount = SHOW_MESS_COUNT
    private var mIsHold2Hand = false
    private var mIsLeft = false
    private var mIsRight = false
    private var mIsSensor = false
    private var curFrontFace: Bitmap? = null
    private var mActivity: Activity? = null
    private lateinit var language: String
    private var finishLiveness = false
    private var cTimer: CountDownTimer? = null

    //---
    constructor(context: Context) : super(context)

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs)

    constructor(context: Context, attributeSet: AttributeSet?, defStyleAttr: Int) : super(context, attributeSet, defStyleAttr)

    override fun onAnimationLineComplete() {}

    override fun onSuccessLineComplete() {
        mMainHandler?.post {
            Log.e("Animation", "onSuccessLineComplete")
            onVerifyFaceListener?.atItem(PointData(mCurItem, isItemSuccess))
        }
    }

    init {
        if (mRootView == null) {
            mRootView = inflate(context, R.layout.control_livenesscheckface_layout, this)
            mVib = context.getSystemService(Context.VIBRATOR_SERVICE) as Vibrator
            mPreviewFullScreen = findViewById(R.id.livenessCheckFaceControl_cameraSourcePreview)
            mGraphicOverlay = findViewById(R.id.livenessCheckFaceControl_graphicOverlay)
            mTrackingFaceControl = findViewById(R.id.livenessCheckFaceControl_gifTracking)

            FirebaseApp.initializeApp(context).apply {
                mFaceDetector = getInstance().getVisionFaceDetector(CameraConfig.MLKitFaceOption)
            }
            var itemActionCount = 0

            show(checkFaceControl_actionBack)

            mTrackingFaceControl?.faceActionPointListener =
                    object : FaceActionPointListener {
                        override fun onActionAngle(item: Int, angle: Float) {
                            if (isStart && !isSleep && item != 0 && isOn) {
                                checkItem(item)
                            }
                            /*if (mNowItem != item) {
                                itemActionCount = 0
                                mNowItem = item
                            }

                            if (isStart && !isSleep && item != 0 && isOn) {
                                if (item == mNowItem) {
                                    itemActionCount++
                                    if (itemActionCount > 1) {
                                        itemActionCount = 0
                                        if (item != mCurItem || mCurItem == mWaitItem) {
                                            checkItem(item)
                                        }
                                    }
                                }
                            }*/
                        }
                    }

            livenessCheckFaceControl_faceControl.initListener(this)
            sensor = Sensor(context, this)
            sensor?.createSensor()
            holdTwoHand()
        }
    }

    fun setActivity(activity: Activity, language: String) {
        this.language = language
        mActivity = activity
        setInchShadown(mActivity!!)
    }

    fun resetTextGuid() {
        liveNessCheckGuid.text = LanguageHelper.getStringLanguage("title_guide")
    }

    fun loadingTextGuid() {
        mActivity?.runOnUiThread {
            liveNessCheckGuid.text = LanguageHelper.getStringLanguage("title_loading")
        }
    }

    private fun startTimer(largestFace: FirebaseVisionFace) {
        cTimer = object : CountDownTimer(1500, 500) {
            override fun onTick(millisUntilFinished: Long) {
                numberTimeOut--
                mActivity?.runOnUiThread { liveNessCheckLabelTimeOut.text = "$numberTimeOut" }
            }

            override fun onFinish() {
                isStart = true
                gone(liveNessCheckLabelTimeOut)
                onVerifyFaceListener?.onResult(arrayBitmap, countFrontFace)
                onVerifyFaceListener?.onStart(isStart, curFrontFace, largestFace.boundingBox)
                mWrongCount = 0
                mStartCount = MAX_START_COUNT
                mFrontFace = largestFace
                if (isVideoRec) mVideoUtils.onStart()
                mActivity?.runOnUiThread {
                    liveNessCheckGuid.text = LanguageHelper.getStringLanguage("title_action")
                }
                cancelTimer()
            }
        }
        cTimer?.start()
    }

    private fun cancelTimer() {
        if (cTimer != null) cTimer?.cancel()
    }

    @SuppressLint("ClickableViewAccessibility")
    private fun holdTwoHand() {
        liveNessCheckFingerLeft.setOnTouchListener { view, event ->
            mIsLeft = event.action == 0 || event.action == 2
            mIsHold2Hand = mIsLeft && mIsRight
            if ((event.action == 0 || event.action == 2) && !leftAnim) {
                leftAnim = true
                liveNessCheckLottieLeft.setAnimation("anim_finger_blue.json")
                liveNessCheckLottieLeft.playAnimation()
            } else if (event.action == 1 && leftAnim) {
                leftAnim = false
                liveNessCheckLottieLeft.setAnimation("anim_finger_red.json")
                liveNessCheckLottieLeft.playAnimation()
            }
            if (!finishLiveness && mIsHold2Hand) {
                liveNessCheckGuid.text = LanguageHelper.getStringLanguage("title_guide")
            }
            return@setOnTouchListener true
        }

        liveNessCheckFingerRight.setOnTouchListener { view, event ->
            mIsRight = (event.action == 0 || event.action == 2)
            mIsHold2Hand = mIsLeft && mIsRight
            if ((event.action == 0 || event.action == 2) && !rightAnim) {
                rightAnim = true
                liveNessCheckLottieRight.setAnimation("anim_finger_blue.json")
                liveNessCheckLottieRight.playAnimation()
            } else if (event.action == 1 && rightAnim) {
                rightAnim = false
                liveNessCheckLottieRight.setAnimation("anim_finger_red.json")
                liveNessCheckLottieRight.playAnimation()
            }
            if (!finishLiveness && mIsHold2Hand) {
                liveNessCheckGuid.text = LanguageHelper.getStringLanguage("title_guide")
            }
            return@setOnTouchListener true
        }
    }

    @SuppressLint("MissingPermission")
    @Suppress("DEPRECATION")
    private fun vibratorAction(mili: Long, isSuccess: Boolean) {
        if (isSuccess) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                mVib?.vibrate(
                        VibrationEffect.createOneShot(
                                mili,
                                VibrationEffect.DEFAULT_AMPLITUDE
                        )
                )
            } else {
                mVib?.vibrate(mili)
            }
        } else {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                val mVibratePattern = longArrayOf(0, mili, 30, mili)
                val effect = VibrationEffect.createWaveform(mVibratePattern, -1)
                mVib?.vibrate(effect)
            } else {
                mVib?.vibrate(mili)
            }
        }

    }

    private fun checkItem(item: Int) {
        isSleep = true
        mCurItem = item
        if (item == mWaitItem) {
            animationItem(item, true)
            vibratorAction(80, true)
        } else {
            animationItem(item, false)
            mWrongCount++
        }
    }

    private fun animationItem(item: Int, isSuccess: Boolean) {
        isItemSuccess = isSuccess
        if (isSuccess) {
            postUI {
                checkFaceControl_notification.notificationSuccess(
                        numberLiveNess,
                        LanguageHelper.getStringLanguage("notification_success_first"),
                        LanguageHelper.getStringLanguage("notification_success_last")
                )
            }
            livenessCheckFaceControl_faceControl.successLine()
        } else {
            postUI {
                checkFaceControl_notification.notificationFail(
                        numberLiveNess,
                        LanguageHelper.getStringLanguage("notification_fail_first"),
                        LanguageHelper.getStringLanguage("notification_fail_last")
                )
            }
            livenessCheckFaceControl_faceControl.failItem()
        }
    }

    private fun initCameraSource() {
        mCameraSource = CameraSource(context, mGraphicOverlay!!)
        mCameraSource?.setFacing(CameraConfig.facing)
        startBackgroundThread()
        mCameraSource?.setFrameProcessorListener(object : FrameStreamListener {
            override fun onFrame(byteArray: ByteArray) {
                frameProcessing(byteArray)
            }

        })
    }

    private fun frameProcessing(frame: ByteArray) {
        mVerifyFaceHandler ?: return
        mCameraSource ?: return
        if (!isOn) return
        if (isVideoRec) mVideoUtils.pushFrame(frame)
        if (isFaceProcessing) return
        isFaceProcessing = true
        if (mIsHold2Hand && mIsSensor) {
            mVerifyFaceHandler?.post {
                val visionImage =
                        FirebaseVisionImage.fromByteArray(frame, CameraConfig.MLKitMetadata)
                faceStatusMess(mFaceStatus)
                mFaceDetector!!.detectInImage(visionImage).addOnSuccessListener { faces ->
                    mVerifyFaceHandler?.post {
                        val largestFace = mFaceUtils.getLargestFace(faces)
                        if (largestFace != null) {
                            if (mCurFaceID != largestFace.trackingId) {
                                mCurFaceID = largestFace.trackingId
                                isStart = false
                                isSleep = false
                                mStartCount = MAX_START_COUNT
                                mFrontCount = MAX_FRONT_COUNT
                                isFaceProcessing = false
                                mCurItem = 0
                                mStartCount = MAX_START_COUNT
                                mFaceStatus = TYPE_FACE_OUT_ZONE
                                onVerifyFaceListener?.onStart(isStart, null, null)
                                onVerifyFaceListener?.onFaceStatus(mFaceStatus)
                            } else {
                                val sizeFace = mFaceUtils.getFaceSize(largestFace)
                                if (sizeFace in CameraConfig.MIN_FACE_SIZE.roundToInt()..CameraConfig.MAX_FACE_SIZE.roundToInt()) {
                                    mAngleZ = getAngleZValue(largestFace)
                                    val isWrongLow = mAngleZ < -5
                                    val isWrongHigh = mAngleZ > 6
                                    val isFrontFace = mFaceUtils.isFrontFace(largestFace)
                                    val isZoneFace = mFaceUtils.checkZoneFace(largestFace)
                                    val isZoneStart = mFaceUtils.checkZoneStartFace(largestFace)
                                    mFaceStatus = if (!isStart) {
                                        if (isZoneFace) {
                                            if (isFrontFace) {
                                                if (isZoneStart) {
                                                    if (!isWrongLow && !isWrongHigh) {
                                                        numberLiveNess = 0
                                                        val isBlur =
                                                                BlurrinessRenderScript.runDetection(
                                                                        context,
                                                                        visionImage.bitmap,
                                                                        largestFace.boundingBox
                                                                )
                                                        if (isBlur) {
                                                            mStartCount--
                                                            if (mStartCount >= 0) {
                                                                curFrontFace = visionImage.bitmap
                                                            }
                                                        }
                                                        if (isStartTimeOut && isBlur) {
                                                            countFrontFace++
                                                            if (arrayBitmap.size <= 5) arrayBitmap.add(
                                                                    visionImage.bitmap
                                                            )
                                                        } else {
                                                            arrayBitmap.clear()
                                                        }
                                                        Log.e("BlurFace", "$isBlur")
                                                        if (!isStartTimeOut) {
                                                            isStartTimeOut = true
                                                            show(liveNessCheckLabelTimeOut)
                                                            startTimer(largestFace)
                                                        }
                                                        setBlurViewControl(null)
                                                        TYPE_FACE_OK
                                                    } else {
                                                        if (isWrongLow) {
                                                            setBlurViewControl(6)
                                                            TYPE_FACE_WRONG_LOW
                                                        } else {
                                                            setBlurViewControl(5)
                                                            TYPE_FACE_WRONG_HIGH
                                                        }
                                                    }
                                                } else {
                                                    setBlurViewControl(3)
                                                    TYPE_FACE_OUT_ZONE
                                                }
                                            } else {
                                                when {
                                                    isWrongHigh -> {
                                                        setBlurViewControl(5)
                                                        TYPE_FACE_WRONG_HIGH
                                                    }
                                                    isWrongLow -> {
                                                        setBlurViewControl(6)
                                                        TYPE_FACE_WRONG_LOW
                                                    }
                                                    else -> {
                                                        setBlurViewControl(4)
                                                        TYPE_FACE_WRONG_FRONT
                                                    }
                                                }
                                            }
                                        } else {
                                            setBlurViewControl(3)
                                            TYPE_FACE_OUT_ZONE
                                        }
                                    } else {
                                        if (isZoneFace) {
                                            if (isFrontFace && isZoneStart && !isWrongHigh && !isWrongLow) {
                                                mFrontCount--
                                                if (mFrontCount <= 0) {
                                                    mFrontCount = MAX_FRONT_COUNT
                                                    mFrontFace = largestFace
                                                    onVerifyFaceListener?.onFrontFace(
                                                            visionImage.bitmap,
                                                            largestFace.boundingBox
                                                    )
                                                }
                                            }
                                            setBlurViewControl(null)
                                            TYPE_FACE_OK
                                        } else {
                                            mFrontCount = MAX_FRONT_COUNT
                                            isStart = false
                                            onVerifyFaceListener?.onStart(isStart, null, null)
                                            mStartCount = MAX_START_COUNT
                                            setBlurViewControl(3)
                                            TYPE_FACE_OUT_ZONE
                                        }
                                    }
                                    if (isStart) {
                                        mTrackingFaceControl!!.setFaceV2(
                                                largestFace,
                                                mFrontFace ?: largestFace,
                                                mAngleZ,
                                                mWaitItem,
                                                mFaceDegree
                                        )
                                    }
                                    onVerifyFaceListener?.onFaceStatus(mFaceStatus)
                                    isFaceProcessing = false
                                } else {
                                    mFaceStatus = if (sizeFace > CameraConfig.MAX_FACE_SIZE) {
                                        setBlurViewControl(2)
                                        TYPE_FACE_OUT_BIG_SIZE
                                    } else {
                                        setBlurViewControl(1)
                                        TYPE_FACE_OUT_SMALL_SIZE
                                    }
                                    mFrontCount = MAX_FRONT_COUNT
                                    isStart = false
                                    onVerifyFaceListener?.onStart(isStart, null, null)
                                    mStartCount = MAX_START_COUNT
                                    onVerifyFaceListener?.onFaceStatus(mFaceStatus)
                                    isFaceProcessing = false
                                }
                            }

                        } else {
                            mFrontCount = MAX_FRONT_COUNT
                            mStartCount = MAX_START_COUNT
                            isStart = false
                            onVerifyFaceListener?.onStart(isStart, null, null)
                            mFaceStatus = TYPE_FACE_NO
                            onVerifyFaceListener?.onFaceStatus(mFaceStatus)
                            setBlurViewControl(0)
                            isFaceProcessing = false
                        }
                    }
                }.addOnFailureListener {
                    isFaceProcessing = false
                }
            }
        } else {
            mFrontCount = MAX_FRONT_COUNT
            mStartCount = MAX_START_COUNT
            isStart = false
            onVerifyFaceListener?.onStart(isStart, null, null)
            mFaceStatus = TYPE_NO_HOLD_2_HANDS
            onVerifyFaceListener?.onFaceStatus(mFaceStatus)
            setBlurViewControl(if (!mIsHold2Hand) 7 else 8)
            livenessCheckFaceControl_faceControl.hideAnim()
            isFaceProcessing = false
        }

    }

    private fun getAngleZValue(largestFace: FirebaseVisionFace): Float {
        try {
            val nosePoint = largestFace.getLandmark(Landmark.NOSE_BASE)
            val eyeLeft = largestFace.getLandmark(Landmark.LEFT_EYE)
            val eyeRight = largestFace.getLandmark(Landmark.RIGHT_EYE)
            val bottomMouth = largestFace.getLandmark(Landmark.BOTTOM_MOUTH)
            val leftMouth = largestFace.getLandmark(Landmark.LEFT_MOUTH)
            val rightMouth = largestFace.getLandmark(Landmark.RIGHT_MOUTH)
            return if (nosePoint != null && eyeLeft != null
                    && eyeRight != null && bottomMouth != null
                    && leftMouth != null && rightMouth != null
            ) {
                val pointNose =
                        Point(nosePoint.position.x.roundToInt(), nosePoint.position.y.roundToInt())
                val pointEyeLeft =
                        Point(eyeLeft.position.x.roundToInt(), eyeLeft.position.y.roundToInt())
                val pointEyeRight =
                        Point(eyeRight.position.x.roundToInt(), eyeRight.position.y.roundToInt())
                val pointMouthLeft =
                        Point(leftMouth.position.x.roundToInt(), leftMouth.position.y.roundToInt())
                val pointMouthRight =
                        Point(rightMouth.position.x.roundToInt(), rightMouth.position.y.roundToInt())

                val pointCenterEye = mFaceUtils.getCenterPoint(pointEyeLeft, pointEyeRight)
                val pointCenterMouth = mFaceUtils.getCenterPoint(pointMouthLeft, pointMouthRight)
                val pointCenterY = mFaceUtils.getCenterPoint(pointCenterEye, pointCenterMouth)

                val pointCenterEyeMouthLeft =
                        mFaceUtils.getCenterPoint(pointEyeLeft, pointMouthLeft)
                val pointCenterEyeMouthRight =
                        mFaceUtils.getCenterPoint(pointEyeRight, pointMouthRight)
                val pointCenterX =
                        mFaceUtils.getCenterPoint(pointCenterEyeMouthLeft, pointCenterEyeMouthRight)

                //------
                val rY = mFaceUtils.distancePoint(pointCenterEye, pointCenterY)
                val disOMY =
                        mFaceUtils.distancePoint(Point(pointCenterY.x, pointNose.y), pointCenterY)
                val angleDataY = disOMY / rY
                val angleY = acos(angleDataY)
                val degreeY =
                        if (pointNose.y < pointCenterY.y) (90 - angleY * (180 / PI).toFloat()) else -(90 - angleY * (180 / PI).toFloat())
                //-------
                val rX = mFaceUtils.distancePoint(pointCenterEyeMouthLeft, pointCenterX)
                val disOMX = mFaceUtils.distancePoint(
                        Point(pointNose.x, pointCenterEyeMouthLeft.y),
                        pointCenterX
                )
                val angleDataX = disOMX / rX
                val angleX = acos(angleDataX)
                val degreeX =
                        if (pointNose.x > pointCenterX.x) (90 - angleX * (180 / PI).toFloat()) else -(90 - angleX * (180 / PI).toFloat())
                //-------
                val zone = if (degreeX < 0 && degreeY > 0) {
                    1
                } else if (degreeX > 0 && degreeY > 0) {
                    2
                } else if (degreeX > 0 && degreeY < 0) {
                    3
                } else {
                    4
                }
                val pointO = Point(pointCenterX.x, pointCenterX.y)
                val degreeData =
                        atan((pointO.y - pointNose.y) / (pointO.x - pointNose.x).toDouble())
                val degreeFace = when (zone) {
                    1 -> {
                        degreeData * 180 / PI
                    }
                    2 -> {
                        180 - abs(degreeData * 180 / PI)
                    }
                    3 -> {
                        180 + degreeData * 180 / PI
                    }
                    4 -> {
                        360 + degreeData * 180 / PI
                    }
                    else -> {
                        0.0
                    }
                }
                mFaceDegree = abs(360 - degreeFace).toFloat()
                degreeY
            } else {
                180f
            }
        } catch (e: Exception) {
            return -180f
        }
    }

    private fun startCameraSource() {
        if (mCameraSource != null) {
            try {
                mPreviewFullScreen?.start(mCameraSource!!, mGraphicOverlay!!)
            } catch (e: IOException) {
                mCameraSource?.release()
                mCameraSource = null
            }
        }
    }

    private fun startBackgroundThread() {
        mVerifyFaceThread = HandlerThread("VerifyFaceThread")
        mModelFaceThread = HandlerThread("BlurFaceThread")
        mVerifyFaceThread?.start()
        mModelFaceThread?.start()
        mModelFaceHandler = Handler(mModelFaceThread?.looper!!)
        mVerifyFaceHandler = Handler(mVerifyFaceThread?.looper!!)
        mMainHandler = Handler(context.mainLooper)
    }

    private fun stopBackgroundThread() {
        mVerifyFaceThread?.quitSafely()
        mVerifyFaceThread = null
        mModelFaceThread?.quitSafely()
        mModelFaceThread = null
    }

    private fun setBlurViewControl(number: Int?) {
        if (!finishLiveness) {
            if (number != null) {
                isStartTimeOut = false
                cancelTimer()
                arrayBitmap.clear()
                numberTimeOut = 4
                countFrontFace = 0
                gone(liveNessCheckLabelTimeOut)
                mActivity?.runOnUiThread {
                    liveNessCheckGuid.text = LanguageHelper.getStringLanguage("title_guide")
                }
            }
        }
        livenessCheckFaceControl_faceControl.setBlurView(number)
    }

    private fun faceStatusMess(status: Int) {
        if (mCameraSource != null) { // check is release
            showMessCount--
            if (status == TYPE_FACE_OK) {
                showMessCount = SHOW_MESS_COUNT
            }
            if (showMessCount < 0) {
                showMessCount = SHOW_MESS_COUNT
                Log.e("", "Status [$status]")
                val textStatus = when (status) {
                    TYPE_FACE_OK -> {
                        "Face OK"
                    }
                    TYPE_FACE_NO -> {
                        setBlurViewControl(0)
                        "No face detected"
                    }
                    TYPE_FACE_OUT_SMALL_SIZE -> {
                        "Move the phone a little closer"
                    }
                    TYPE_FACE_OUT_BIG_SIZE -> {
                        "Move the phone a little further"
                    }
                    TYPE_FACE_OUT_ZONE -> {
                        "No face in the detection area"
                    }
                    TYPE_FACE_WRONG_FRONT -> {
                        "Move the phone facing your face"
                    }
                    TYPE_FACE_WRONG_HIGH -> {
                        "Move the phone a little higher"
                    }
                    TYPE_FACE_WRONG_LOW -> {
                        "Move the phone a little closer"
                    }
                    TYPE_NO_HOLD_2_HANDS -> {
                        "Hold phone with two\nhand in front of you"
                    }
                    else -> ""
                }

                mMainHandler?.post {
                    if (status != TYPE_FACE_OK) {
                        setBlurViewControl(0)
                        livenessCheckFaceControl_faceControl.hideAnim()
                    }
                }
            }
        }
    }

    fun initControl(listener: OnVerifyFaceListener, isRecVideo: Boolean) {
        liveNessCheckGuid.text = LanguageHelper.getStringLanguage("title_guide")
        liveNessCheckLabelLeft.text = LanguageHelper.getStringLanguage("thumb_left")
        liveNessCheckLabelRight.text = LanguageHelper.getStringLanguage("thumb_right")
        val hasPer = context.checkSelfPermission(android.Manifest.permission.CAMERA)
        if (hasPer == PERMISSION_GRANTED) {
            createCameraSource()
            onVerifyFaceListener = listener
            startActionCamera()
            isVideoRec = isRecVideo
        } else {
            Log.e(TAG, "No camera permission access")
        }
    }

    fun resumeControl() {
        leftAnim = false
        rightAnim = false
        liveNessCheckLottieRight.setAnimation("anim_finger_red.json")
        liveNessCheckLottieRight.playAnimation()
        liveNessCheckLottieLeft.setAnimation("anim_finger_red.json")
        liveNessCheckLottieLeft.playAnimation()
        val hasPer = context.checkSelfPermission(android.Manifest.permission.CAMERA)
        if (hasPer == PERMISSION_GRANTED) {
            createCameraSource()
            startActionCamera()
        } else {
            Log.e(TAG, "No camera permission access")
        }
    }

    fun pauseControl() {
        releaseCamera()
    }

    fun destroyControl() {
        sensor?.destroySensor()
        destroyVideoRec()
        releaseCanvas()
        cancelTimer()
    }

    fun waitItem(item: ByteArray?) {
        livenessCheckFaceControl_faceControl.showLine()
        isSleep = false
        mWaitItem = if (item != null) getPointData(item)!!.position else 0
        numberLiveNess++
        when (mWaitItem) {
            1 -> {
                livenessCheckFaceControl_faceControl.topLeft()
            }
            2 -> {
                livenessCheckFaceControl_faceControl.centerLeft()
            }
            3 -> {
                livenessCheckFaceControl_faceControl.botLeft()
            }
            4 -> {
                livenessCheckFaceControl_faceControl.topRight()
            }
            5 -> {
                livenessCheckFaceControl_faceControl.centerRight()
            }
            6 -> {
                livenessCheckFaceControl_faceControl.botRight()
            }
            7 -> {
                livenessCheckFaceControl_faceControl.top()
            }
            8 -> {
                livenessCheckFaceControl_faceControl.bot()
            }
        }
        livenessCheckFaceControl_faceControl.setImgPoint(mWaitItem)
    }

    fun finishVerifyAnim(bm: Bitmap?) {
        mActivity!!.runOnUiThread {
            gone(livenessCheckLayoutFinger)
            livenessCheckFaceControl_faceControl.completeLiveness(bm)
        }

    }

    fun resumeVerifyAnim() {
        show(livenessCheckLayoutFinger)
        livenessCheckFaceControl_faceControl.resetAfterComplete()
        startVerify()
        mFrontCount = MAX_FRONT_COUNT
        mStartCount = MAX_START_COUNT
        isStart = false
        onVerifyFaceListener?.onStart(isStart, null, null)
        mFaceStatus = TYPE_NO_HOLD_2_HANDS
        onVerifyFaceListener?.onFaceStatus(mFaceStatus)
        setBlurViewControl(7)
        livenessCheckFaceControl_faceControl.hideAnim()
        isFaceProcessing = false
    }

    fun startVerify() {
        isOn = true
        isStart = false
        isSleep = false
        mStartCount = MAX_START_COUNT
        mFrontCount = MAX_FRONT_COUNT
        isFaceProcessing = false
        mCurItem = 0

    }

    fun getVideoFilePath(listener: VideoRecProcessListener) {
        if (isVideoRec) {
            mVideoUtils.onDone(listener)
        } else {
            listener.onDone("")
        }
    }

    fun pauseRecVideo() {
        mVideoUtils.onPause()
    }

    private fun releaseCamera() {
        if (mCameraSource != null) {
            mCameraSource?.release()
            mPreviewFullScreen?.release()
            mCameraSource = null
            mFaceDetector?.close()
            stopBackgroundThread()
            mVib?.cancel()
        }
    }

    private fun releaseCanvas() {
        livenessCheckFaceControl_faceControl.onDestroy()
    }

    private fun createCameraSource() {
        val isGranted = context.checkSelfPermission(android.Manifest.permission.CAMERA)
        if (isGranted == PERMISSION_GRANTED) {
            if (mCameraSource == null) {
                initCameraSource()
            }
        } else {
            Log.e(TAG, "Error: No Camera Permission")
        }
    }

    private fun startActionCamera() {
        livenessCheckFaceControl_faceControl.startHandlerThread()
        startCameraSource()
    }

    private fun getPointData(str: ByteArray?): PointData? {
        str ?: return null
        return Gson().fromJson(String(str), PointData::class.java)
    }

    private fun setInchShadown(activity: Activity) {
        livenessCheckFaceControl_faceControl.setInchShadow(activity)
    }

    fun checkFaceControlChangeLogo(logo: Int) {
        checkFaceControl_logo.setImageResource(logo)
    }

    fun checkFaceControlOnBack(listener: OnClickListener) {
        checkFaceControl_actionBack.setOnClickListener { listener.onClick(it) }
    }

    fun goneActionBack(){
        gone(checkFaceControl_actionBack)
    }

    private fun destroyVideoRec() {
        mVideoUtils.onDestroy()
    }

    override fun checkSensor(bool: Boolean) {
        mIsSensor = bool
    }
}