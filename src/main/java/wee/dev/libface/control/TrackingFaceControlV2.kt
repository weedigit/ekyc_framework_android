package wee.dev.libface.control

import android.content.Context
import android.graphics.Point
import android.graphics.PointF
import android.graphics.Rect
import android.os.Handler
import android.os.Looper
import android.util.AttributeSet
import android.util.Log
import android.util.Size
import android.view.View
import androidx.constraintlayout.widget.ConstraintLayout
import com.google.firebase.ml.vision.face.FirebaseVisionFace
import kotlinx.android.synthetic.main.control_trackingface_layout.view.*
import wee.dev.libface.R
import wee.dev.libface.base.FaceActionPointListener
import wee.dev.libface.camera.CameraConfig
import kotlin.math.*


class TrackingFaceControlV2 : ConstraintLayout {

    private var mRootView: ConstraintLayout? = null

    private var mGifContainer: ConstraintLayout? = null

    private var mTrackingView: ConstraintLayout? = null

    private var mCurFace: FirebaseVisionFace? = null

    private var cX = 0f

    private var cY = 0f

    private var isProcessing = false

    private var scrW = 0f

    private var scrH = 0f

    var faceActionPointListener: FaceActionPointListener? = null

    private var mCurPlusY = 0f

    private var mCurAngleY = 0f

    private var mHandler = Handler(Looper.myLooper()!!)

    constructor(context: Context) : super(context)

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs)

    constructor(context: Context, attributeSet: AttributeSet?, defStyleAttr: Int) : super(context, attributeSet, defStyleAttr)

    init {
        if (mRootView == null) {
            mRootView = inflate(context, R.layout.control_trackingface_layout, this) as ConstraintLayout
            mRootView!!.post {
                mGifContainer = mRootView!!.findViewById(R.id.trackingFaceControl_gifContainer)
                mTrackingView = mRootView!!.findViewById(R.id.trackingFaceControl_tracking)
                scrH = height.toFloat()
                scrW = width.toFloat()
                mHandler = Handler(Looper.getMainLooper())
            }

        }
    }

    fun setFaceV2(face: FirebaseVisionFace, frontFace: FirebaseVisionFace, angleZ: Float, waitItem: Int,faceDegree: Float){
        if(isProcessing) return
        isProcessing = true
        mHandler.post {
            val rectFrontFace = frontFace.boundingBox
            val centerPoint = PointF(rectFrontFace.exactCenterX(),rectFrontFace.exactCenterY())
            val rectFace = face.boundingBox
            val centerFace = PointF(rectFace.exactCenterX(),rectFace.exactCenterY())
            val angle = if(centerFace.x < centerPoint.x){
                if(centerFace.y < centerPoint.y){
                    /*Left Up*/
                    calculateQuadantIV(centerFace,centerPoint)
                }else{
                    /*Left Down*/
                    calculateQuadantI(centerFace,centerPoint)
                }
            }else{
                if(centerFace.y < centerPoint.y){
                    /*Right Up*/
                    calculateQuadantIII(centerFace,centerPoint)

                }else{
                    /*Right Down*/
                    calculateQuadantII(centerFace,centerPoint)

                }
            }
            val dis2Point = distancePoint(centerPoint,centerFace)
            val eulerAngleY = face.headEulerAngleY
            val degreeFace = radianToDegree(angle)
            val zoneY = -12f..12f
            val zoneZ = -6.5f..7f
            val zoneUpDown =/* -7f..11f*/-6.5f..7f
            val zoneLeftRight = -12f..12f
            val actionCheck = when(waitItem){
                1 -> {eulerAngleY !in zoneY && angleZ !in zoneZ}
                2 -> {eulerAngleY !in zoneLeftRight}
                3 -> {eulerAngleY !in zoneY && angleZ !in zoneZ}
                4 -> {eulerAngleY !in zoneY && angleZ !in zoneZ}
                5 -> {eulerAngleY !in zoneLeftRight}
                6 -> {eulerAngleY !in zoneY && angleZ !in zoneZ}
                7 -> {angleZ !in zoneUpDown}
                8 -> {angleZ !in zoneUpDown}
                else -> false
            }

            val disMin = face.boundingBox.width()*0.10

            if(actionCheck && dis2Point>/*45*/disMin){
                val item1 = when (degreeFace) {
                   /* in 12.5f..67.5f -> {/*Right Bottom*/6 }
                    in 112.5f..167.5f -> {/*Left Bottom*/3 }
                    in 167.5f..202.5f -> {/*Left Center*/2 }
                    in 202.5f..247.5f -> {/*Left Top*/1 }
                    in 292.5f..337.5f -> {/*Right Top*/4 }
                    in 337.5f..360f -> {/*Right Center*/5 }
                    in 0f..12.5f -> {/*Right Center*/5 }
                    in 67.5f..112.5f -> {/*Center Bottom*/8 }
                    in 247.5f..292.5f -> {/*Center Top*/7 }
                    else -> {/*Out*/0 }*/
                    in 90f..270f -> 2
                    else -> 5
                }

                /*val item2 = when (faceDegree){
                    in 12.5f..67.5f -> {/*Right Bottom*/6 }
                    in 112.5f..167.5f -> {/*Left Bottom*/3 }
                    in 167.5f..202.5f -> {/*Left Center*/2 }
                    in 202.5f..247.5f -> {/*Left Top*/1 }
                    in 292.5f..337.5f -> {/*Right Top*/4 }
                    in 337.5f..360f -> {/*Right Center*/5 }
                    in 0f..12.5f -> {/*Right Center*/5 }
                    in 67.5f..112.5f -> {/*Center Bottom*/8 }
                    in 247.5f..292.5f -> {/*Center Top*/7 }
                    else -> {/*Out*/0 }
                    in 90..270 -> 2
                    else -> 5
                }*/
                /*val item = if(item1==waitItem) item1 else item2*/
                //Log.e("AngleFace","[$degreeFace] - [$faceDegree] - [${face.headEulerAngleY}] - [$angleZ] - [${dis2Point}]")
                faceActionPointListener?.onActionAngle(item1, degreeFace)
            }
            isProcessing = false
        }

    }

    private fun getRectView(view: View): Rect {
        return Rect(
            view.x.roundToInt(),
            view.y.roundToInt(),
            view.width + view.x.roundToInt(),
            view.height + view.y.roundToInt()
        )
    }

    private fun distancePoint(a : PointF, b: PointF): Float{
        return sqrt((a.x.toDouble() - b.x.toDouble()).pow(2.0) + (a.y.toDouble() - b.y.toDouble()).pow(2.0)).toFloat()
    }

    private fun calculateQuadantI(center: PointF, point: PointF): Float{
        val r = distancePoint(center,point)
        val h = abs(point.y - center.y)
        val angle = asin(h/r)
        return angle
    }

    private fun calculateQuadantII(center: PointF, point: PointF): Float{
        val r = distancePoint(center,point)
        val h = abs(point.y - center.y)
        val angle = PI.toFloat() - asin(h/r)
        return angle
    }

    private fun calculateQuadantIII(center: PointF, point: PointF): Float{
        val r = distancePoint(center,point)
        val h = abs(point.y - center.y)
        val angle = PI.toFloat() + asin(h/r)
        return angle
    }

    private fun calculateQuadantIV(center: PointF, point: PointF): Float{
        val r = distancePoint(center,point)
        val h = abs(point.y - center.y)
        val angle = 2f*PI.toFloat() - asin(h/r)
        return angle
    }

    private fun radianToDegree(radian: Float): Float{
        return radian * (180/PI).toFloat()
    }

    /**
     * Adjusts the y coordinate from the preview's coordinate system to the view coordinate system.
     */
    private fun translateY(y: Float): Float {
        return scaleY(y)
    }
    private fun translateX(x: Float): Float {
        return mRootView!!.width - scaleX(x)
    }
    private fun scaleX(horizontal: Float): Float {
        return horizontal * 1.0f * CameraConfig.scaleX
    }
    private fun scaleY(vertical: Float): Float {
        return vertical * 1.0f * CameraConfig.scaleY
    }


}