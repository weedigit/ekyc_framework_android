package wee.dev.libface.control

import android.content.Context
import android.content.res.ColorStateList
import android.os.Handler
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import kotlinx.android.synthetic.main.notification_view.view.*
import wee.dev.libface.R
import wee.dev.libface.util.extension.animationTop

class NotificationView : ConstraintLayout {

    private val myHandler = Handler()

    private val runnable = Runnable {
        animationTop(
                notification_root,
                0f,
                -notification_root.height.toFloat() * 2,
                300
        ) {}
    }

    constructor(context: Context) : super(context) {
        initView(context, null)
    }

    constructor(context: Context, attributeSet: AttributeSet?) : super(context, attributeSet) {
        initView(context, attributeSet)
    }

    constructor(context: Context, attributeSet: AttributeSet?, defStyleAttr: Int) : super(
            context,
            attributeSet,
            defStyleAttr
    ) {
        initView(context, attributeSet)
    }

    private fun initView(context: Context, attributeSet: AttributeSet?) {
        LayoutInflater.from(context).inflate(R.layout.notification_view, this)

        notification_root.translationY = -notification_root.height.toFloat() * 2
    }

    fun notificationSuccess(number: Int = 0, labelFirst: String = "", labelLast: String = "") {
        handler.removeCallbacks(runnable)
        notification_root.backgroundTintList =
                ColorStateList.valueOf(ContextCompat.getColor(context, R.color.middle_blue))
        notification_icon.setImageResource(R.drawable.ic_noti_success)
        notification_label.text = "$labelFirst $number $labelLast"
        notification_root.visibility = View.VISIBLE
        animationTop(notification_root, -notification_root.height.toFloat() * 2, 0f, 300) {}
        hideAnim()
    }

    fun notificationFail(number: Int = 0, labelFirst: String = "", labelLast: String = "") {
        handler.removeCallbacks(runnable)
        notification_root.backgroundTintList =
                ColorStateList.valueOf(ContextCompat.getColor(context, R.color.alert))
        notification_icon.setImageResource(R.drawable.ic_noti_fail)
        notification_label.text = "$labelFirst $number $labelLast"
        notification_root.visibility = View.VISIBLE
        animationTop(notification_root, -notification_root.height.toFloat() * 2, 0f, 300) {}
        hideAnim()
    }

    private fun hideAnim() {
        myHandler.postDelayed(runnable, 1000)
    }

}